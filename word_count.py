from clear_text import collect_files



if __name__ == '__main__':
    target_dir = """/media/ubuntu/data/combined/german_cropped"""
    txt_files = collect_files(target_dir, ".txt")
    words = set()
    words_total = 0
    for f in txt_files:

        with open(f) as file:
            line = file.readline()
            line = line.replace("Text: ", "")
            line = line.replace("\n", "")
            line = line.lower()
            words_total += len(line.split())
            for w in line.split():
                words.add(w)

    print(words_total)
    print(len(words))
