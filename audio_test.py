import speech_recognition as sr
from speech_recognition import AudioData
from copy import copy
import json
import ast
from time import sleep

class MyAudioData(AudioData):

    def __init__(self, frame_data, sample_rate, sample_width):
        super(MyAudioData, self).__init__(frame_data, sample_rate, sample_width)

    @staticmethod
    def from_ad(ad:AudioData):
        return MyAudioData(frame_data=ad.frame_data, sample_rate=ad.sample_rate, sample_width=ad.sample_width)

    def get_segment(self, start_ms=None, end_ms=None):
        """
        Returns a new ``AudioData`` instance, trimmed to a given time interval. In other words, an ``AudioData`` instance with the same audio data except starting at ``start_ms`` milliseconds in and ending ``end_ms`` milliseconds in.

        If not specified, ``start_ms`` defaults to the beginning of the audio, and ``end_ms`` defaults to the end.
        """
        assert start_ms is None or start_ms >= 0, "``start_ms`` must be a non-negative number"
        if start_ms is None:
            start_byte = 0
        else:
            start_byte = int((start_ms * self.sample_rate * self.sample_width) // 1000)
        if end_ms is None:
            end_byte = len(self.frame_data)
        else:
            end_byte = int((end_ms * self.sample_rate * self.sample_width) // 1000)
        if end_byte < 0:
            end_byte = len(self.frame_data) + end_byte
        return MyAudioData(self.frame_data[start_byte:end_byte], self.sample_rate, self.sample_width)


def write_audio(audio, target_path):
    with open(target_path, "wb") as file:
        file.write(audio.get_wav_data())


def replace(text:str):
    if isinstance(text, tuple):
        text = list(text)

    if isinstance(text, list):

        temp = list()
        for x in text:
            temp.append(replace(x))
        return temp

    if text is None:
        return text
    text = text.replace("-", " ")
    text = text.replace(":", " ")
    text = text.replace("€", "euro")
    text = text.lower()
    return text


def translate(recognizer, seg, language, emergency_step_size, retries=3, retry_sleep=5, emergency_retries=3):
    step = 0
    text = None
    success = False

    for x in range(retries + emergency_retries):
        try:
            # print("text recognizer: asking")
            text = recognizer.recognize_google(seg, language=language, show_all=True)
            # print("text recognizer: done")

            if isinstance(text, dict):
                temp = text["alternative"]
            elif isinstance(text, list) and len(text) > 0:
                temp = text[0]
            else:
                temp = None

            if temp is not None:
                text = list()

                for t in temp:
                    if isinstance(t, dict) and "transcript" in t.keys():
                        text.append(t["transcript"])
                    success = True

        except Exception as e:
            # print("error: " + str(e))
            if x >= retries:

                if emergency_step_size >= 0:

                    seg = seg.get_segment(emergency_step_size)
                else:

                    seg = seg.get_segment(0, -emergency_step_size)

                step += emergency_step_size

            # print("retry")
        if success:
            break
        elif retry_sleep is not None:
            sleep(retry_sleep)

    if not success:
        try:
            text = recognizer.recognize_google(seg, language=language, show_all=False)
        except Exception as e:
            # print("error: " + str(e))
            text = None


    return seg, text, step


def get_text_with_conf(recognizer, seg, language, emergency_step_size, retries=3):

    step = 0
    text = None
    conf = None
    for x in range(retries):

        try:

            text = recognizer.recognize_google(seg, language=language, show_all=True)
            temp = text["alternative"]

            for t in temp:
                if "transcript" in t.keys() and "confidence" in t.keys():
                    text = t["transcript"]
                    conf = int(t["confidence"] * 10)

        except Exception as e:
            # print("error: " + str(e))

            if emergency_step_size >= 0:
                seg = seg.get_segment(emergency_step_size)

            else:
                seg = seg.get_segment(0, -emergency_step_size)

            step += emergency_step_size
            # print("retry")

        if conf is not None and text is not None:
            break

    return text, conf


def get_max_word_count(text:str):
    if isinstance(text, list):
        cw = 0
        for t in text:
            cw = max(cw, get_max_word_count(t))
        return cw
    return len(text.split())


def shorten_anchor(audio_anchor, text_anchor, recognizer, language):
    test_audio = copy(audio_anchor)

    start = None

    step_size = 100

    min_step_size = 50
    max_step_size = 200

    text = None
    found_text = None

    start_offset = 0

    max_start_dif = 5

    min_word_cound = 3

    # print("anker:", text_anchor)
    for x in range(100):
        seg = test_audio.get_segment(step_size)

        seg, text, translate_step = translate(recognizer, seg, language, min_step_size, 3)
        if text is None:
            if step_size < min_step_size:
                # print("no text 1")
                return None, None, None, None
            else:
                step_size = int(step_size / 2)
                continue
        elif get_max_word_count(text) < min_word_cound:
            # print("low words out")
            return None, None, None, None

        start_offset += translate_step

        text = replace(text)
        # print(text)

        if found_text is not None:

            temp_text = ends_with(text_anchor, text)

            if temp_text is None:
                break

            if len(temp_text) > len(found_text) and temp_text.endswith(found_text):

                audio_anchor = test_audio
                text_anchor = temp_text
                test_audio = seg
                start_offset += step_size

            elif found_text != temp_text:

                break

            else:

                audio_anchor = test_audio
                text_anchor = temp_text
                test_audio = seg
                start_offset += step_size
                # print("up 0", str(len(test_audio.frame_data)))
        else:

            if start is None:
                start, index = get_first_match(text_anchor, text)

                test_audio = seg
                start_offset += step_size

                if start is not None and start > max_start_dif:
                    start = None

                # if start is not None:
                #     print(start)
                #     print(text_anchor.split()[start])

            else:

                temp = ends_with(text_anchor, text)

                if temp is not None:

                    if len(temp.split()) + start == len(text_anchor.split()):

                        found_text = temp
                        test_audio = seg
                        start_offset += step_size
                        step_size = min_step_size

                    else:

                        test_audio = seg
                        start_offset += step_size

                else:

                    temp, index = get_first_match(text_anchor, text)

                    if temp is not None and temp > start:

                        step_size = int(step_size / 2)

                        if step_size < min_step_size:
                            if start + 1 >= len(text_anchor.split()):
                                return None, None, None, None

                            # print("start inc 0")
                            step_size = max_step_size
                            start += 1
                            # print(start)
                            # print(text_anchor.split()[start])
                    else:

                        test_audio = seg
                        start_offset += step_size

    if found_text is None:
        # print("no text found...")
        return None, None, None, None

    step_size = max_step_size
    test_audio = copy(audio_anchor)
    found_text = None
    start = None

    end_offset = 0

    # print(text_anchor)

    for x in range(100):
        seg = test_audio.get_segment(0, -step_size)

        seg, text, translate_step = translate(recognizer, seg, language, -min_step_size, 3)

        if text is None:
            if step_size < min_step_size:
                # print("no text 1")
                return None, None, None, None
            else:
                step_size = int(step_size / 2)
                continue
        elif get_max_word_count(text) < min_word_cound:
            # print("low words out")
            return None, None, None, None

        end_offset -= translate_step

        if text is None or len(text) == 0:
            return None, None, None, None

        text = replace(text)
        # print(text)

        if found_text is not None:
            temp_text = starts_with(text_anchor, text)

            if temp_text is None:
                break

            if len(temp_text) > len(found_text) and temp_text.startswith(found_text):

                test_audio = seg
                end_offset += step_size
                # print("up 1")

            elif found_text != temp_text:
                break

            else:

                test_audio = seg
                end_offset += step_size
        else:

            if start is None:

                start, index = get_last_match(text_anchor, text)

                test_audio = seg
                end_offset += step_size

                if start is not None and start > max_start_dif:
                    start = None

                # if start is not None:
                #     print(start)
                #     print(text_anchor.split()[-start])

            else:

                temp = starts_with(text_anchor, text)

                if temp is not None:

                    if len(temp.split()) + start - 1 == len(text_anchor.split()):

                        found_text = temp
                        step_size = min_step_size
                    else:

                        test_audio = seg
                        end_offset += step_size
                else:

                    temp, index = get_last_match(text_anchor, text)

                    if temp is not None and temp > start:

                        step_size = int(step_size / 2)

                        if step_size < min_step_size:

                            if start + 1 >= len(text_anchor.split()):
                                return None, None, None, None

                            step_size = max_step_size
                            start += 1

                    else:

                        test_audio = seg

                        end_offset += step_size

    if found_text is None:
        return None, None, None, None

    # print("start", start_offset / 1000)
    # print("end", -end_offset / 1000)

    text, conf = get_text_with_conf(recognizer, test_audio, language, min_step_size, 3)
    write_audio(test_audio, """/media/ubuntu/data/lip_reading_data/temp/test.wav""")
    return test_audio, (text, conf), start_offset, -end_offset


def speech_to_text(audio_target, language, simple=False):
    r = sr.Recognizer()

    with sr.AudioFile(audio_target) as source:
        audio_anchor = r.listen(source)
        audio_anchor = MyAudioData.from_ad(audio_anchor)

    if simple:
        text, conf = get_text_with_conf(r, audio_anchor, language, 5, 3)
        return audio_anchor, (text, conf), 0, 0


    text_anchor = None
    for _ in range(3):
        try:
            audio_anchor, text_anchor, _ = translate(r, audio_anchor, language, 5)
        except:
            # print("retrying")
            pass

    if text_anchor is None:
        # print("no text 0")
        return None, None, None, None

    text_anchor = replace(text_anchor)
    # print(text_anchor)

    for ta in text_anchor:
        temp = shorten_anchor(audio_anchor, ta, r, language)
        if temp is None:
            continue
        ok = True
        for t in temp:
            if t is None:
                ok = False
                break
        if ok:
            return temp


def starts_with(text_anchor:str, new_text:str):
    if isinstance(new_text, list):
        for x in new_text:
            if text_anchor.startswith(x):
                return x
        return None
    if text_anchor.startswith(new_text):
        return new_text
    else:
        return None


def ends_with(text_anchor:str, new_text:str):
    if isinstance(text_anchor, list):
        for ta in text_anchor:
            temp = ends_with(ta, new_text)
            if temp is not None:
                return temp
    elif isinstance(new_text, list):
        for x in new_text:
            if text_anchor.endswith(x):
                return x
    elif text_anchor.endswith(new_text):
        return new_text

    return None


def get_last_match(text_anchor:str, new_text:str):
    if isinstance(text_anchor, list):
        first = None
        index = None

        for ta in text_anchor:
            temp, i = get_last_match(ta,  new_text)

            if temp is not None and (first is None or temp < first):
                first = temp
                index = i

        return first, index

    if isinstance(new_text, list):
        last = None
        index = None

        for x, n_t in enumerate(new_text):

            temp = get_last_match(text_anchor, n_t)
            if temp is not None and (last is None or temp < last):
                last = temp
                index = x

        return last, index

    text_anchor_list = text_anchor.split()
    new_text_list = new_text.split()
    x = 0
    found_dif = False

    for x in range(len(text_anchor_list)):
        if x >= len(new_text_list):
            return None

        if new_text_list[x] != text_anchor_list[x]:
            found_dif = True
            break
    x -= 1

    if not found_dif:
        return None
    else:
        return len(text_anchor_list) - x


def get_first_match(text_anchor:str, new_text:str):
    if isinstance(text_anchor, list):
        first = None
        index = None

        for ta in text_anchor:
            temp, i = get_first_match(ta,  new_text)

            if temp is not None and (first is None or temp < first):
                first = temp
                index = i

        return first, index

    elif isinstance(new_text, list):
        first = None
        index = None

        for x, n_t in enumerate(new_text):

            temp = get_first_match(text_anchor, n_t)
            if temp is not None and (first is None or temp < first):
                first = temp
                index = x

        return first, index

    text_anchor_list = text_anchor.split()
    new_text_list = new_text.split()
    if text_anchor.endswith(new_text):
        return len(text_anchor_list) - len(new_text_list)
    x = 0
    found_dif = False

    for x in range(len(text_anchor_list)):
        if x >= len(new_text_list):
            # print("hier")
            return None

        if new_text_list[-1 -x] != text_anchor_list[-1 -x]:
            found_dif = True
            break

    x -= 1
    if not found_dif:
        return None
    else:
        return len(new_text_list) - x

def blub():
    wav_path = """/media/ubuntu/data/lip_reading_data/temp/60QwETU5WtM_S21.wav"""
    print(speech_to_text(wav_path, "de-DE", simple=True))



if __name__ == '__main__':
    blub()