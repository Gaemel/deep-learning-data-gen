

class Textifier():

    SPECIAL = "special"

    def __init__(self, language="de"):
        self._lang = language

        self._number_dict = dict()
        self._number_dict["de"] = dict()
        self._number_dict["de"][self.SPECIAL] = {"0": "null", "1": "eins", "11": "elf", "12": "zwölf", "1000": "ein tausend", "1000000": "eine millionen", "1000000000": "eine milliarge", "1000000000000": "eine billion"}
        self._number_dict["de"][1] = {"0": "", "1": "ein", "2": "zwei", "3": "drei", "4": "vier", "5": "fünf", "6": "sechs", "7": "sieben", "8": "acht", "9": "neun"}
        self._number_dict["de"][2] = {"1": "zehn", "2": "zwanzig", "3": "dreißig", "4": "vierzig", "5": "fünfzig", "6": "sechszig", "7": "siebzig", "8": "achtzig", "9": "neunzig"}
        self._number_dict["de"][3] = "hundert"
        self._number_dict["de"][4] = "tausend"
        self._number_dict["de"][7] = "millionen"
        self._number_dict["de"][10] = "milliarge"
        self._number_dict["de"][13] = "billion"

        self._special_char_replacement_list = [
            ("ü", "ue"), ("ö", "oe"), ("ä", "ae"), ("ß", "ss"),
        ]

    def replace_special_chars(self, text:str):
        for s, n in self._special_char_replacement_list:
            text = text.replace(s, n)
        return text

    def is_str_float(self, word:str):

        if word.count(",") == 1:
            a, b = word.split(",")
        elif word.count(".") == 1:
            a, b = word.split(".")
        else:
            return False

        return str(a).isdigit() and str(b).isdigit()

    def write_out_number(self, word):
        if not isinstance(word, str):
            word = str(word)

        if self.is_str_float(word):
            a, b = word.replace(",", ".").split(".")
            return self._write_out_number(a) + " komma " + self._write_out_number_mantissa(b)

        return self._write_out_number(word)

    def _remove_leading_zeros(self, word, ignore_last=True):
        if ignore_last:
            end = 1
        else:
            end = 0

        while len(word) > end and word[0] == "0":
            word = word[1:]
        return word

    def _write_out_number_mantissa(self, word):
        res = ""
        for w in word:
            res += self._write_out_number(w)
        return res

    def _write_out_number(self, word, use_special=True):
        word = self._remove_leading_zeros(word)

        if word == "":
            return ""

        if self._lang == "de":
            if use_special and word in self._number_dict[self._lang][self.SPECIAL].keys():
                return self._number_dict[self._lang][self.SPECIAL][word]

            if len(word) == 1:
                return self._number_dict[self._lang][1][word]
            elif len(word) == 2:
                n = word[0]
                m = word[1]

                temp = self._write_out_number(m, False)
                if temp != "" and n != "1":
                    temp += "und"
                temp += self._number_dict[self._lang][2][n]

                return temp
            elif len(word) == 3:
                n = word[0]
                m = word[1:]

                temp = self._write_out_number(n) + self._number_dict[self._lang][3]
                temp += self._write_out_number(m)

                return temp
            else:
                i = 0
                while True:
                    n = word[0:i+1]
                    m = word[i+1:]
                    if (len(m) + 1) in self._number_dict[self._lang].keys():
                        break
                    else:
                        i += 1
                temp = self._write_out_number(n) + self._number_dict[self._lang][len(m) + 1]
                m = self._remove_leading_zeros(m, ignore_last=False)
                temp += self._write_out_number(m)
                return temp

    def is_number(self, word:str):
        return word.isdigit() or self.is_str_float(word)

    def convert(self, some_text):
        res = ""
        for word in some_text.split():
            if self.is_number(word):
                res += self.write_out_number(word)
            else:
                res += word

            res += " "
        res = res[:-1]

        res = self.replace_special_chars(res)

        return res

def blub_2():
    ntt = Textifier()
    x = 1234.0056
    print(x, ntt.write_out_number(x))

def blub_1():
    ntt = Textifier()
    for x in range(10**15, 10**16, 3141113713):
        print(x, ntt.write_out_number(x))

def blub_0():
    a = "das ist ein test mit der Zahl 43239 und der zahl 4,5"
    print(a)
    print(Textifier().convert(a))

if __name__ == '__main__':
    blub_0()