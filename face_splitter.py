import commen
import cv2
import face_recognition
import sys
import os
from multiprocessing import Pool
from functools import partial
import shutil

NO_FACE = 0
SINGULAR_FACE = 1
GROUP_FACE = 2

def split_singular_faces_and_groups(meta_file, watch_interval=5, image_scale=2):
    scene = commen.load_scene(meta_file)

    face_code = NO_FACE

    start_index = None
    for c, frame in enumerate(scene):

        if c % watch_interval != 0:
            continue

        rgb_frame = frame[:, :, ::-1]
        small_frame = cv2.resize(rgb_frame, (0, 0), fx=1 / image_scale, fy=1 / image_scale)
        face_locations = face_recognition.face_locations(small_frame)

        if start_index is None and len(face_locations) > 0:
            start_index = c

        if len(face_locations) == 1:
            face_code = SINGULAR_FACE
        elif len(face_locations) > 1:
            face_code = GROUP_FACE
            break

        # for top, right, bottom, left in face_locations:
        #     top *= image_scale
        #     right *= image_scale
        #     bottom *= image_scale
        #     left *= image_scale
        #
        #     cv2.rectangle(frame, (left, top), (right, bottom), (255, 0, 255), 1)
        # cv2.imshow("test", frame)
        # cv2.waitKey(1)

    # print(face_code)
    # print(str(data["video"]) + ": " + str(face_code))
    return face_code, start_index


def sorter(meta_files, face_codes, start_index_list):
    group_list = list()
    singular_list = list()

    # print(start_index_list)

    for i in range(len(face_codes)):
        start_index = start_index_list[i]
        if face_codes[i] == SINGULAR_FACE:
            singular_list.append((meta_files[i], start_index))
        elif face_codes[i] == GROUP_FACE:
            group_list.append((meta_files[i], start_index))

    return singular_list, group_list


def cut(target_dir, meta_file, start_index):
    if start_index is None or start_index == 0:
        return

    meta_data = commen.load_config(meta_file)
    # fps = meta_data[commen.KEY_VIDEO_FPS]

    scene = commen.load_scene(meta_file)
    new_scene = scene[start_index:]

    meta_data[commen.KEY_START] = meta_data[commen.KEY_START] + start_index
    meta_file_name, _ = os.path.splitext(os.path.split(meta_file)[-1])
    commen.save(target_dir, meta_file_name, meta=meta_data, video=new_scene)

def run(config_path, target):

    my_key = "face_splitter"
    data = commen.load_config(config_path, my_key)

    worker_count = data["sub_process_count"] if "sub_process_count" in data else 1
    watch_interval = data["watch_interval"] if "watch_interval" in data else 5
    image_scale = data["image_scale"] if "image_scale" in data else 2
    remove_old = data[commen.KEY_REMOVE_OLD] if commen.KEY_REMOVE_OLD in data else False

    parent_name = os.path.split(target)[-1]

    output_dir = os.path.join(data["output_dir"], parent_name)

    meta_files = [os.path.join(target, x) for x in os.listdir(target) if str(x).endswith(".yaml")]

    if not os.path.exists(target):
        return

    ssfag_with_args = partial(split_singular_faces_and_groups, watch_interval=watch_interval, image_scale=image_scale)

    with Pool(processes=worker_count) as pool:
        content = pool.map(ssfag_with_args, meta_files)

        face_codes = list()
        start_index_list = list()

        for f, s in content:
            face_codes.append(f)
            start_index_list.append(s)

    if face_codes is not None:

        singular, group = sorter(meta_files, face_codes, start_index_list)

        singular_dir = None
        group_dir = None

        for meta, start_index in singular:

            temp = commen.copy(meta, output_dir, copy_og_vid=True)
            if singular_dir is None:
                singular_dir = temp
            # cut(temp, meta, start_index)

        for meta, start_index in group:

            temp = commen.copy(meta, output_dir, copy_og_vid=True)
            if group_dir is None:
                group_dir = temp

    if remove_old:
        try:
            shutil.rmtree(target)
        except:
            pass


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("need exactly 2 args (config-path) and (taget_file)")

    config_path = sys.argv[1]
    target_file = sys.argv[2]

    run(config_path, target_file)
