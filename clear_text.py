import os

def collect_files(parent_dir, file_ending=".txt"):
    res_list = list()
    for title in os.listdir(parent_dir):
        sub_dir = os.path.join(parent_dir, title)
        if os.path.isdir(sub_dir):
            res_list += collect_files(sub_dir, file_ending)
        elif sub_dir.endswith(file_ending):
            res_list.append(sub_dir)
    return res_list

def has_none_conf(target_path):
    has = False
    with open(target_path, "r") as f:
        temp = f.readline()
        while temp is not None and temp != "":
            if temp == "Conf: None":
                has = True
                break
            temp = f.readline()
    return has

def run(parent_dir):
    for p in collect_files(parent_dir):
        needs_removing = has_none_conf(p)
        if needs_removing:
            print("removing:", str(p))
            os.remove(p)
    print("done")


if __name__ == '__main__':
    target = """/media/ubuntu/data/lip_reading_data/text"""
    run(target)