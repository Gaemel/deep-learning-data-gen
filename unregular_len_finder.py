import commen
from clear_text import collect_files

def is_len_regular(meta_file):
    meta = commen.load_meta(meta_file)
    vid = commen.load_scene(meta_file)

    if meta is None:
        return False

    if "start" not in meta or "end" not in meta:
        return False

    meta_len = int(meta["end"] - meta["start"])

    if vid is None:
        return False

    vid_len = len(vid)
    if vid_len != meta_len:
        print(vid_len, meta_len)
    return vid_len == meta_len


def main():
    target = """/media/ubuntu/data/lip_reading_data/text"""

    meta_paths = collect_files(target, ".yaml")

    for m in meta_paths:

        if not is_len_regular(m):
            print(m)

    print("done")

if __name__ == '__main__':
    main()