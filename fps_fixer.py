import commen
import os
from clear_text import collect_files
import cv2


def list_vid_fps(parent_dir, black_list=None):

    for file_path in collect_files(parent_dir, ".mp4"):
        cap = cv2.VideoCapture(file_path)
        fps = int(cap.get(cv2.CAP_PROP_FPS))
        if black_list is None or fps not in black_list:
            print(fps, file_path)
        cap.release()


def fix_vid(meta_path):
    fps = int(commen.get_fps(meta_path))
    if fps == 25:
        return

    scene = commen.load_scene(meta_path)
    if scene is None:
        return

    for x in range(len(scene)-1, 0, -2):
        scene.pop(x)

    target_dir, file_name = os.path.split(meta_path)
    file_name = os.path.splitext(file_name)[0]
    meta = commen.load_meta(meta_path)
    meta[commen.KEY_VIDEO_FPS] = 25.0
    commen.save(target_dir, file_name, meta, video=scene)
    print("updated: " + file_name)


def main(parent_dir):
    meta_list = collect_files(parent_dir, ".yaml")
    for m in meta_list:
        fix_vid(m)


def blub():
    parent_path = """/media/ubuntu/data/lip_reading_data/downloads"""

    list_vid_fps(parent_path, [25])


def some_test():
    target = """/media/ubuntu/data/lip_reading_data/temp/1T7P-4-fQXw_S25.yaml"""
    fix_vid(target)


if __name__ == '__main__':
    blub()
