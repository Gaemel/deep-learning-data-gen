import cv2


def show_len_of_clip(target_vid):
    cap = cv2.VideoCapture(target_vid)
    i = 0
    while True:
        s, f = cap.read()
        if not s:
            break
        else:
            i += 1

    cap.release()
    return i


def main():
    target = """/media/ubuntu/data/lip_reading_data/text/1bzvno5GzoE/1bzvno5GzoE_S1173.mp4"""
    print(show_len_of_clip(target))


if __name__ == '__main__':
    main()
