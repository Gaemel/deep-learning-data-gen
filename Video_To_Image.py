import cv2
from os import path

haar_face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')

def detect_faces(f_cascade, colored_img, scaleFactor=1.1):
  # just making a copy of image passed, so that passed image is not changed
  img_copy = colored_img.copy()
  # convert the test image to gray image as opencv face detector expects gray images
  gray = cv2.cvtColor(img_copy, cv2.COLOR_BGR2GRAY)
  # let's detect multiscale (some images may be closer to camera than others) images
  faces = f_cascade.detectMultiScale(gray, scaleFactor=scaleFactor, minNeighbors=5)
  # go over list of faces and draw them as rectangles on original colored img
  for (x, y, w, h) in faces:
    cv2.rectangle(img_copy, (x, y), (x + w, y + h), (0, 255, 0), 2)
  return img_copy, len(faces) > 0

vidcap = cv2.VideoCapture('tagesschau_000.mp4')

success, image = vidcap.read()
count = 1

target_shape = (180, 100)

dir_path = """/home/ubuntu/PycharmProjects/Video_Data_Miner/image_data"""
print(haar_face_cascade)


while success:
  temp_path = path.join(dir_path, "image_%d.jpg" % count)
  image = cv2.resize(image, target_shape)
  image, face_detected = detect_faces(haar_face_cascade, image)

  if face_detected:
    cv2.imwrite(temp_path, image)
    count += 1
    print(count)
  success, image = vidcap.read()

print("done")


