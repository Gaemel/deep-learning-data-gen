import cv2
import os


def get_video_len(target):
    cap = cv2.VideoCapture(target)

    i = 0
    s = True
    while s:
        s, _ = cap.read()
        i += 1

    return i


def find_max_value(parent_path, verbose=True):
    bucket_list = list()

    sub_list = os.listdir(parent_path)
    p = -1
    for n, sub in enumerate(sub_list):

        if verbose:
            current_p = int(100 / len(sub_list) * n)

            if current_p > p:
                p = current_p
                print(str(p) + "% ")
                for i in range(len(bucket_list)):
                    print(i, bucket_list[i])
                print()

        temp_path = os.path.join(parent_path, sub)

        if os.path.isdir(temp_path):
            temp_bucket = find_max_value(temp_path, verbose=False)
            for i in range(len(temp_bucket)):

                while len(bucket_list) <= i:
                    bucket_list.append(0)

                bucket_list[i] += temp_bucket[i]

        elif os.path.isfile(temp_path) and os.path.splitext(temp_path)[-1] == ".mp4":
            i = get_video_len(temp_path)

            i = int(i)
            while len(bucket_list) <= i:
                bucket_list.append(0)
            bucket_list[i] += 1

    return bucket_list


def max_len_remover(parent_dir, max_len=25*5):

    for sub in os.listdir(parent_dir):
        temp_path = os.path.join(parent_dir, sub)
        if os.path.isdir(temp_path):
            max_len_remover(temp_path, max_len)
        elif os.path.isfile(temp_path) and os.path.splitext(temp_path)[-1] == ".mp4":
            if get_video_len(temp_path) > max_len:
                os.remove(temp_path)
                print("removed", temp_path)


def blub():
    print(find_max_value("""/media/ubuntu/data/combined/pre_train"""))

def main():
    target = """/media/ubuntu/data/combined/temp"""
    max_len_remover(target, max_len=25*1)

if __name__ == '__main__':
    main()

