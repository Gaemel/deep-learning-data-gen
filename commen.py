import yaml
import os
import cv2
import shutil
import moviepy.editor
from audio_test import MyAudioData

def write_video():
    pass

def write_audio():
    pass

def write_text():
    pass

def write_meta_file():
    pass

KEY_ORIGINAL_VIDEO = "og_vid"

MAX_TIME = "max_time"

# The other files
KEY_OUTPUT_DIR = "output_dir"
KEY_INPUT_DIR = "input_dir"

KEY_DOWNLOAD_LIST = "download_list"

KEY_VIDEO_FORMAT = "video_format"
KEY_VIDEO = "video"
KEY_TEXT = "text"
KEY_AUDIO = "audio"

KEY_LANGUAGE = "language"

KEY_REMOVE_OLD = "remove_old"
KEY_USE_SIMPLE_STT = "use_simple"

# The start and end stamp in the og-video
KEY_START = "start"
KEY_END = "end"

KEY_VIDEO_FPS = "video_fps"
KEY_VIDEO_ENDING = "video_ending"

__DEFAULT_META = [""]

some_dict = {"test": 1, "blub": "bla"}
some_dir = "./temp"
file_name = "some_file"


def load_scene(meta_file):
    with open(meta_file) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

    if not os.path.exists(data["video"]):
        return None

    cap = cv2.VideoCapture(data["video"])
    scene = list()

    # print("loading: " + str(data["video"]))
    while True:
        s, f = cap.read()
        if not s:
            break
        scene.append(f)
    cap.release()
    return scene

def load_meta(meta_file):
    with open(meta_file) as f:
        meta = yaml.load(f, Loader=yaml.FullLoader)
    return meta

def get_fps(meta_file):
    data = load_config(meta_file)

    if KEY_VIDEO_FPS not in data:

        cap = cv2.VideoCapture(data["video"])
        fps = cap.get(cv2.CAP_PROP_FPS)
        data[KEY_VIDEO_FPS] = fps

        with open(meta_file, "w") as file:
            yaml.dump(data, file)

        return fps

    return data[KEY_VIDEO_FPS]

def load_config(config_path, my_key=None):
    with open(config_path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

    if my_key is None:
        pass
    elif isinstance(my_key, list):
        for key in my_key:
            data = data[key]
    else:
        data = data[my_key]

    return data

def remove(meta_file, remove_og_video=False):
    data = load_config(meta_file)
    os.remove(data[KEY_VIDEO])
    os.remove(meta_file)
    if remove_og_video:
        if os.path.exists(data[KEY_ORIGINAL_VIDEO]):
            os.remove(data[KEY_ORIGINAL_VIDEO])

def copy(meta_file, target_dir, copy_og_vid=False):
    return move(meta_file, target_dir, copy_og_vid, keep_old=True)

def move(meta_file, target_dir, copy_og_vid=False, keep_old=False):
    os.makedirs(target_dir, exist_ok=True)

    # print("meta: " + str(meta_file))
    with open(meta_file) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

    og_video = data[KEY_ORIGINAL_VIDEO]
    new_og_video = os.path.join(target_dir, os.path.split(og_video)[-1])

    video_file = data[KEY_VIDEO]
    new_video_file = os.path.join(target_dir, os.path.split(video_file)[-1])

    new_meta_file = os.path.join(target_dir, os.path.split(meta_file)[-1])

    if KEY_AUDIO in data:
        audio_path = data[KEY_AUDIO]
        new_audio_path = os.path.join(target_dir, os.path.split(audio_path)[-1])
        data[audio_path] = new_audio_path
        if keep_old:
            shutil.copy(audio_path, new_audio_path)
        else:
            os.rename(audio_path, new_audio_path)

    data[KEY_ORIGINAL_VIDEO] = new_og_video
    data[KEY_VIDEO] = new_video_file

    if keep_old:
        shutil.copy(meta_file, new_meta_file)
        shutil.copy(video_file, new_video_file)
    else:
        os.rename(meta_file, new_meta_file)
        os.rename(video_file, new_video_file)

    with open(new_meta_file, "w") as f:
        yaml.dump(data, f)

    if copy_og_vid and not os.path.exists(new_og_video):
        shutil.copy(og_video, new_og_video)

    return os.path.split(new_meta_file)[0]


def save(target_dir, file_name, meta:dict, video=None, text=None, audio=None, verbose=False, target_fps=25, *args, **kwargs):
    os.makedirs(target_dir, exist_ok=True)

    if video is not None:
        video_path = _save_vid(video, target_dir, file_name, verbose, *args, **kwargs)
        meta[KEY_VIDEO] = video_path

    if audio is not None:
        audio_path = _save_audio(audio, target_dir, file_name, verbose, *args, **kwargs)
        meta[KEY_AUDIO] = audio_path

    if text is not None:
        text_path = _save_text(text, target_dir, file_name, verbose, *args, **kwargs)
        meta[KEY_TEXT] = text_path

    meta_path = os.path.join(target_dir, str(file_name) + ".yaml")

    with open(meta_path, "w") as file:
        if verbose:
            print("save meta at: " + str(meta_path))
        yaml.dump(meta, file)

    return meta_path

def _save_text(text, target_dir, file_title, verbose=False, *args, **kwargs):
    temp_path = os.path.join(target_dir, str(file_title) + ".txt")
    if isinstance(text, tuple):
        with open(temp_path, "w") as file:
            file.write("Text: " + str(text[0]) + "\n")
            file.write("Conf: " + str(text[1]))
    else:
        with open(temp_path, "w") as file:
            file.write(text)
    if verbose:
        print("save text at: " + str(temp_path))

    return temp_path

def _save_audio(audio, target_dir, file_title, verbose=False, *args, **kwargs):

    audio_ext = ".wav" if KEY_VIDEO_ENDING not in kwargs else kwargs[KEY_VIDEO_ENDING]
    temp_path = os.path.join(target_dir, str(file_title) + audio_ext)

    if isinstance(audio, MyAudioData):
        with open(temp_path, "wb") as file:
            wav_data = audio.get_wav_data()
            file.write(wav_data)
    else:
        audio.write_audiofile(temp_path, verbose=False)

    if verbose:
        print("save audio at:" + str(temp_path))
    return temp_path

def _save_vid(video, target_dir, file_title, verbose=False, *args, **kwargs):

    video_fps = 25 if KEY_VIDEO_FPS not in kwargs else kwargs[KEY_VIDEO_FPS]
    ext = ".mp4" if KEY_VIDEO_ENDING not in kwargs else kwargs[KEY_VIDEO_ENDING]

    if ext == ".avg":
        fourcc = cv2.VideoWriter_fourcc(*"XVID")
    else:
        ext = ".mp4"
        fourcc = cv2.VideoWriter_fourcc(*"mp4v")

    height, wigth = video[0].shape[:-1]

    shape = (wigth, height)
    temp_path = os.path.join(target_dir, str(file_title) + ext)
    out = cv2.VideoWriter(temp_path, fourcc, video_fps, shape)
    if verbose:
        print("save vid at: " + str(temp_path))

    for frame in video:
        out.write(frame)

    out.release()
    return temp_path

if __name__ == '__main__':
    save(some_dir, file_name, some_dict)