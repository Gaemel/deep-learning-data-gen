import numpy as np
import cv2
import face_recognition
import sys
import os
import yaml
import commen
from multiprocessing import Pool
from functools import partial
import shutil


def identify_all_faces(frames, watch_interval=5, image_scale=2):
    known_face_encodings = list()
    pos_list = list()
    face_first_frame = list()

    for c, frame in enumerate(frames):
        if c % watch_interval != 0:
            continue

        rgb_frame = frame[:, :, ::-1]
        small_frame = cv2.resize(rgb_frame, (0, 0), fx=1 / image_scale, fy=1 / image_scale)
        face_locations = face_recognition.face_locations(small_frame)
        face_encodings = face_recognition.face_encodings(small_frame, face_locations)

        for i, face_encoding in enumerate(face_encodings):
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

            if matches.count(True) == 0:
                top, right, bottom, left = face_locations[i]
                rgb_new_face_image = small_frame[top:bottom, left:right]

                temp = face_recognition.face_encodings(rgb_new_face_image)

                for new_face_encoding in temp:
                    known_face_encodings.append(new_face_encoding)
                    face_first_frame.append(c)
                    pos_list.append([np.array(face_locations[i]) * image_scale])
                matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            if len(face_distances) <= 0:
                continue

            best_match_index = np.argmin(face_distances)

            if matches.count(True) != 0 and matches[best_match_index]:
                pos_list[best_match_index].append(np.array(face_locations[i]) * image_scale)


    return pos_list, face_first_frame


def cut_section_old(frames, locations, start_frame, up_scale=1.2, buffer_size=5, over_size_scale_steps=0.1, target_size=100, *args, **kwargs):

    top_index = 0
    right_index = 1
    bottom_index = 2
    left_index = 3

    locations = np.array(locations)

    max_height = (locations[:, bottom_index] - locations[:, top_index]).max()
    max_width = (locations[:, right_index] - locations[:, left_index]).max()

    max_height = int(min(max_height*up_scale, frames[0].shape[0]))
    max_width = int(min(max_width*up_scale, frames[0].shape[1]))

    max_height = max(max_height, max_width)
    max_width = max(max_height, max_width)

    center_height = int((locations[:, bottom_index].mean() + locations[:, top_index].mean()) / 2)
    center_width = int((locations[:, left_index].mean() + locations[:, right_index].mean()) / 2)

    top = int(center_height - max_height / 2)
    right = int(center_width + max_width / 2)
    bottom = int(center_height + max_height / 2)
    left = int(center_width - max_width / 2)

    if top < 0 or left < 0 or bottom >= frames[0].shape[0] or right >= frames[0].shape[1]:
        return cut_section(frames, locations, start_frame, up_scale - over_size_scale_steps, buffer_size,
                           over_size_scale_steps)

    new_frames = list()

    for frame in frames[start_frame:]:
        section = frame[top:bottom, left:right]
        section = cv2.resize(section, (target_size, target_size))
        new_frames.append(section)

        print(section.shape)
        cv2.imshow("test", section)
        cv2.waitKey(10)

    return new_frames

def smooth(kernel_size, array):

    kernel = np.ones(kernel_size) / kernel_size

    start_padding = list()
    end_padding = list()
    for i in range(int((kernel_size-1) / 2)):
        start_padding.append(array[0])
        end_padding.append(array[-1])

    temp_array = np.concatenate([np.array(start_padding), array, np.array(end_padding)])

    return np.convolve(temp_array, kernel, mode='valid')

def cut_section(frames, locations, start_frame, up_scale=1.2, buffer_size=5, over_size_scale_steps=0.1, target_size=100, offset=0.0, *args, **kwargs):

    top_index = 0
    right_index = 1
    bottom_index = 2
    left_index = 3

    kernel_size = 5
    kernel = np.ones(kernel_size) / kernel_size

    # locations[:, top_index] = np.convolve(locations[:, top_index], kernel, mode='same')
    # locations[:, right_index] = np.convolve(locations[:, right_index], kernel, mode='same')
    # locations[:, bottom_index] = np.convolve(locations[:, bottom_index], kernel, mode='same')
    # locations[:, left_index] = np.convolve(locations[:, left_index], kernel, mode='same')

    locations = np.array(locations)

    max_height = (locations[:, bottom_index] - locations[:, top_index]).max()
    max_width = (locations[:, right_index] - locations[:, left_index]).max()

    max_height = int(min(max_height*up_scale, frames[0].shape[0]))
    max_width = int(min(max_width*up_scale, frames[0].shape[1]))

    max_height = max(max_height, max_width)
    max_width = max(max_height, max_width)

    mouth_offset = int(max_height * offset)

    height_offset = int(max_height/2)
    width_offset = int(max_width/2)

    center_heights = (locations[:, top_index] + locations[:, bottom_index]) / 2
    center_widths = (locations[:, left_index] + locations[:, right_index]) / 2

    center_heights = smooth(9, center_heights)
    center_widths = smooth(9, center_widths)

    # center_height = int((locations[:, bottom_index].mean() + locations[:, top_index].mean()) / 2)
    # center_width = int((locations[:, left_index].mean() + locations[:, right_index].mean()) / 2)

    new_frames = list()

    # print(len(frames))
    # print(start_frame)
    # print(len(locations))

    for i, frame in enumerate(frames[start_frame:]):

        if i >= len(locations):
            break

        center_height = int(center_heights[i])
        center_width = int(center_widths[i])

        top = int(center_height - height_offset + mouth_offset)
        right = int(center_width + width_offset)
        bottom = int(center_height + height_offset + mouth_offset)
        left = int(center_width - width_offset)

        if top < 0:
            bottom -= top
            top = 0
        if bottom >= frame.shape[0]:
            top += frame.shape[0] - bottom
            if top < 0:
                return None

        if left < 0:
            right -= left
            left = 0
        if right >= frame.shape[1]:
            left += frame.shape[1] - right
            if left < 0:
                return None

        section = frame[top:bottom, left:right]
        section = cv2.resize(section, (target_size, target_size))
        new_frames.append(section)

        # print(section.shape)
        # cv2.imshow("test", section)
        # cv2.waitKey(int(1000/25))

    return new_frames


def cut_singular_moving_faces(meta_file, output_dir, config_path, config_key):
    scene = commen.load_scene(meta_file)
    fps = commen.get_fps(meta_file)
    meta_data = commen.load_config(meta_file)

    config = commen.load_config(config_path, config_key)

    watch_interval = config["watch_interval"] if "watch_interval" in config else 5
    image_scale = config["image_scale"] if "image_scale" in config else 2
    movement_threshold_image = config["movement_threshold_image"] if "movement_threshold_image" in config else 0.05
    movement_threshold_video = config["movement_threshold_video"] if "movement_threshold_video" in config else 0.8
    offset = config["offset_vertical"] if "offset_vertical" in config else 0.0

    pos_list, spawn_index = identify_all_faces(scene, 1, image_scale)

    singular_moving_face = None

    # target_spawn_index = 0
    # target_end_index = 0
    #
    # print()
    # print(meta_file)

    target_spawn_index = 0
    target_end_index = 0
    mean_list = list()
    for i in range(len(pos_list)):
        section_list = cut_section(scene, pos_list[i], spawn_index[i], offset=offset, **config)
        if section_list is None:
            continue

        past_frame = None
        movement_count = 0

        for c, section in enumerate(section_list):
            if c % watch_interval != 0:
                continue

            gray = cv2.cvtColor(section, cv2.COLOR_BGR2GRAY)
            if past_frame is not None:
                change = cv2.absdiff(gray, past_frame)
                mean_list.append(change.mean())
                if change.mean() >= movement_threshold_image:
                    movement_count += 1

            past_frame = gray

        # print("movement_count * watch_interval / len(section_list) " + str(movement_count * watch_interval / len(section_list)))
        if movement_count * watch_interval / len(section_list) >= movement_threshold_video:

            if singular_moving_face is not None:
                # print("second moving face")
                singular_moving_face = None
                break
            else:
                # print("first moving face")
                target_spawn_index = spawn_index[i]
                target_end_index = len(section_list)
                singular_moving_face = section_list

    if singular_moving_face is None:
        return

    meta_data[commen.KEY_START] = meta_data[commen.KEY_START] + target_spawn_index
    meta_data[commen.KEY_END] = meta_data[commen.KEY_START] + target_end_index
    meta_data[commen.KEY_ORIGINAL_VIDEO] = os.path.join(output_dir, os.path.split(meta_data[commen.KEY_ORIGINAL_VIDEO])[-1])
    meta_file_name, _ = os.path.splitext(os.path.split(meta_file)[-1])
    commen.save(target_dir=output_dir, file_name=meta_file_name, meta=meta_data, video=singular_moving_face, verbose=False)


def run(config_path, target):
    my_key = "face_slicer"
    data = commen.load_config(config_path, my_key)

    remove_old = data[commen.KEY_REMOVE_OLD] if commen.KEY_REMOVE_OLD in data else False
    worker_count = data["sub_process_count"] if "sub_process_count" in data else 1

    if not os.path.exists(target):
        print("(slicer) target does not exist: " + str(target))
        return

    meta_files = [os.path.join(target, x) for x in os.listdir(target) if str(x).endswith(".yaml")]
    if len(meta_files) >= 1:
        og_video = commen.load_config(meta_files[0])[commen.KEY_ORIGINAL_VIDEO]

        out_dir = os.path.join(data["output_dir"], os.path.splitext(os.path.split(og_video)[-1])[0])

        new_og_video = os.path.join(out_dir, os.path.split(og_video)[-1])
        os.makedirs(out_dir, exist_ok=True)

        if not os.path.exists(new_og_video):
            shutil.copy(og_video, new_og_video)

        csmf_with_args = partial(cut_singular_moving_faces, output_dir=out_dir, config_path=config_path, config_key=my_key)
        with Pool(processes=worker_count) as pool:
            pool.map(csmf_with_args, meta_files)

    if remove_old:
        try:
            shutil.rmtree(target)
        except:
            pass

def some_test():
    meta_file = "/home/ubuntu/PycharmProjects/Video_Data_Miner/group/tagesschau_000/tagesschau_000_S32.yaml"
    output_dir = "/home/ubuntu/PycharmProjects/Video_Data_Miner/temp"
    config_path = "/home/ubuntu/PycharmProjects/Video_Data_Miner/config.yaml"
    config_key = "slicer"

    cut_singular_moving_faces(meta_file, output_dir, config_path, config_key)


def some_other_test():
    meta_file = """/media/ubuntu/data/lip_reading_data/faces/JYUsc-v5bhY/JYUsc-v5bhY_S2.yaml"""
    out_dir = """/media/ubuntu/data/lip_reading_data/temp"""

    config_path = """/home/ubuntu/PycharmProjects/lip-reading-data-miner/config.yaml"""

    my_key = "face_slicer"

    cut_singular_moving_faces(meta_file, out_dir, config_path, my_key)

def main():
    if len(sys.argv) != 3:
        print("need exactly 2 args (config-path) and (taget_file)")

    config_path = sys.argv[1]
    target_file = sys.argv[2]

    run(config_path, target_file)

if __name__ == '__main__':
    some_other_test()
