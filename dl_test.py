import youtube_dl
import time

"""youtube_dl --format "bestvideo[ext=mp4][vcodec!*=av01]+bestaudio[ext=m4a] """

# youtube_dl.YoutubeDL.format_resolution()

target_file = "./targets.txt"
ydl = youtube_dl.YoutubeDL({'outtmpl': './%(id)s.%(ext)s', "format": "bestvideo[ext=mp4][vcodec!*=av01]+bestaudio[ext=m4a]"})


taget_url = "https://www.youtube.com/watch?v=1m0VEAfLV4E"
with ydl:
    result = ydl.extract_info(
        taget_url,
        download=True # We just want to extract the info
    )

if 'entries' in result:
    # Can be a playlist or a list of videos
    video = result['entries'][0]
else:
    # Just a video
    video = result

print(video)
video_url = video['title']
print(video_url)

# while True:
#     content = list()
#     with open(target_file, "r+") as file:
#         while True:
#             temp = file.readline()
#             temp = temp.replace("\n", "")
#             # print(temp)
#             if temp is None or temp == "":
#                 break
#             content.append(temp)
#         file.seek(0)
#         # file.truncate()
#
#     if len(content) > 0:
#         with ydl:
#             for c in content:
#                 result = ydl.extract_info(
#                     c,
#                     download=True # We just want to extract the info
#                 )
#                 temp = result["id"] + "." + result["ext"]
#                 print()
#     else:
#         print("start sleep")
#         time.sleep(10)
#         print("end sleep")