import os
import shutil
import sys
import pyaudio
import speech_recognition as sr
from multiprocessing import Pool
from functools import partial
from audio_test import speech_to_text as stt
import commen


def speech_to_text(meta_file, output_dir, language, simple=False):
    # print("text process on: ", str(meta_file))
    meta_data = commen.load_config(meta_file)
    audio_path = meta_data[commen.KEY_AUDIO]

    audio = None
    text = None
    start_offset = None
    end_offset = None

    # try:
    #     audio, text, start_offset, end_offset = stt(audio_path)
    # except:
    #     print("text failed at: " + str(meta_file))

    temp = stt(audio_path, language=language, simple=simple)
    if temp is not None:
        audio, text, start_offset, end_offset = temp
    else:
        # print("none")
        return

    if audio is None or text is None:
        # print("none")
        return

    meta_file_name, _ = os.path.splitext(os.path.split(meta_file)[-1])
    commen.copy(meta_file, output_dir, False)

    if simple:
        commen.save(target_dir=output_dir, file_name=meta_file_name, text=text, verbose=False, meta=meta_data)
    else:
        scene = commen.load_scene(meta_file)
        fps = meta_data[commen.KEY_VIDEO_FPS]

        start_offset = int(start_offset * fps / 1000)
        end_offset = int(end_offset * fps / 1000)

        scene = scene[start_offset:end_offset]
        meta_data[commen.KEY_START] = meta_data[commen.KEY_START] + start_offset
        meta_data[commen.KEY_START] = meta_data[commen.KEY_END] - end_offset

        meta_data[commen.KEY_ORIGINAL_VIDEO] = os.path.join(output_dir, os.path.split(meta_data[commen.KEY_ORIGINAL_VIDEO])[-1])
        meta_data[commen.KEY_VIDEO] = os.path.join(output_dir, meta_file_name + ".mp4")

        commen.save(target_dir=output_dir, file_name=meta_file_name, meta=meta_data, text=text, video=scene, audio=audio, verbose=False)

    # with sr.AudioFile(audio_path) as source:
    #     audio = r.listen(source)
    # try:
    #     text = r.recognize_google(audio, language=language)
    #
    #     meta_data[commen.KEY_ORIGINAL_VIDEO] = os.path.join(output_dir,
    #                                                         os.path.split(meta_data[commen.KEY_ORIGINAL_VIDEO])[-1])
    #
    #     meta_file_name, _ = os.path.splitext(os.path.split(meta_file)[-1])
    #
    #     commen.copy(meta_file, output_dir, False)
    #     meta_data[commen.KEY_VIDEO] = os.path.join(output_dir, meta_file_name + ".mp4")
    #     commen.save(target_dir=output_dir, file_name=meta_file_name, meta=meta_data, text=text)
    # except:
    #     print("text failed at: " + str(meta_file))


def run(config_path, target):
    my_key = "text_creator"
    data = commen.load_config(config_path, my_key)

    simple = data[commen.KEY_USE_SIMPLE_STT] if commen.KEY_USE_SIMPLE_STT in data else True
    remove_old = data[commen.KEY_REMOVE_OLD] if commen.KEY_REMOVE_OLD in data else False
    worker_count = data["sub_process_count"] if "sub_process_count" in data else 1

    if not os.path.exists(target):
        print("(text) target does not exist: " + str(target))
        return
    meta_files = [os.path.join(target, x) for x in os.listdir(target) if str(x).endswith(".yaml")]

    if len(meta_files) >= 1:

        og_video = commen.load_config(meta_files[0])[commen.KEY_ORIGINAL_VIDEO]

        out_dir = os.path.join(data["output_dir"], os.path.splitext(os.path.split(og_video)[-1])[0])
        new_og_video = os.path.join(out_dir, os.path.split(og_video)[-1])
        os.makedirs(out_dir, exist_ok=True)

        if not os.path.exists(new_og_video):
            shutil.copy(og_video, new_og_video)

        language = data[commen.KEY_LANGUAGE]

        stt_with_args = partial(speech_to_text, output_dir=out_dir, language=language, simple=simple)
        with Pool(processes=worker_count) as pool:
            pool.map(stt_with_args, meta_files)

    if remove_old:
        try:
            shutil.rmtree(target)
        except:
            pass

def complete_dir():
    target_dir = """/media/ubuntu/data/lip_reading_data/audio"""
    config = "./config.yaml"

    print(os.listdir(target_dir))
    for sub_dir in os.listdir(target_dir):
        temp_path = os.path.join(target_dir, sub_dir)
        print(temp_path)
        run_in_main(config, temp_path)

def target_dir():
    target_dir = """/media/ubuntu/data/lip_reading_data/audio/YUwRLG9CeMQ"""
    config = "./config.yaml"

    run(config, target_dir)

def run_in_main(config_path, target):
    print("target:", target)
    if not os.path.isdir(target):
        return

    my_key = "text_creator"
    data = commen.load_config(config_path, my_key)

    remove_old = data[commen.KEY_REMOVE_OLD] if commen.KEY_REMOVE_OLD in data else False
    worker_count = data["sub_process_count"] if "sub_process_count" in data else 1

    if not os.path.exists(target):
        print("(text) target does not exist: " + str(target))
        return
    meta_files = [os.path.join(target, x) for x in os.listdir(target) if str(x).endswith(".yaml")]

    if len(meta_files) >= 1:

        og_video = commen.load_config(meta_files[0])[commen.KEY_ORIGINAL_VIDEO]

        out_dir = os.path.join(data["output_dir"], os.path.splitext(os.path.split(og_video)[-1])[0])
        new_og_video = os.path.join(out_dir, os.path.split(og_video)[-1])
        os.makedirs(out_dir, exist_ok=True)

        if not os.path.exists(new_og_video):
            shutil.copy(og_video, new_og_video)

        language = data[commen.KEY_LANGUAGE]

        for mf in meta_files:
            speech_to_text(mf, output_dir=out_dir, language=language)

        # stt_with_args = partial(speech_to_text, output_dir=out_dir, language=language)
        # with Pool(processes=worker_count) as pool:
        #     pool.map(stt_with_args, meta_files)

    if remove_old:
        try:
            shutil.rmtree(target)
        except:
            pass

def main():
    if len(sys.argv) != 3:
        print("need exactly 2 args (config-path) and (taget_file)")

    config_path = sys.argv[1]
    target = sys.argv[2]

    run(config_path, target)


if __name__ == '__main__':
    complete_dir()

