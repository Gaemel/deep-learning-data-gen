

target_file = """/media/ubuntu/data/lip_reading_data/faces/done.txt"""

my_list = list()
with open(target_file, "r") as file:
    temp = file.readline()
    while temp is not None and temp != "":
        if temp not in my_list:
            my_list.append(temp)
        temp = file.readline()

with open(target_file, "w") as file:
    for line in my_list:
        file.write(line)
