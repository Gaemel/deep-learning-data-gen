import numpy as np
import cv2
import face_recognition
import yaml
import os
import sys


def identify_all_faces(frames, watch_interval=5, image_scale=2):
    known_face_encodings = list()
    pos_list = list()
    face_first_frame = list()

    for c, frame in enumerate(frames):
        if c % watch_interval != 0:
            continue

        rgb_frame = frame[:, :, ::-1]
        small_frame = cv2.resize(rgb_frame, (0, 0), fx=1 / image_scale, fy=1 / image_scale)
        face_locations = face_recognition.face_locations(small_frame)
        face_encodings = face_recognition.face_encodings(small_frame, face_locations)

        for i, face_encoding in enumerate(face_encodings):
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

            if matches.count(True) == 0:
                top, right, bottom, left = face_locations[i]
                rgb_new_face_image = small_frame[top:bottom, left:right]

                temp = face_recognition.face_encodings(rgb_new_face_image)

                for new_face_encoding in temp:
                    known_face_encodings.append(new_face_encoding)
                    face_first_frame.append(c)
                    pos_list.append([np.array(face_locations[i]) * image_scale])
                matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            if len(face_distances) <= 0:
                continue

            best_match_index = np.argmin(face_distances)

            if matches.count(True) != 0 and matches[best_match_index]:
                pos_list[best_match_index].append(np.array(face_locations[i]) * image_scale)

    return pos_list, face_first_frame


def cut_section(frames, locations, start_frame, up_scale=1.5, buffer_size=5, over_size_scale_steps=0.1):

    top_index = 0
    right_index = 1
    bottom_index = 2
    left_index = 3

    locations = np.array(locations)

    max_height = (locations[:, bottom_index] - locations[:, top_index]).max()
    max_width = (locations[:, right_index] - locations[:, left_index]).max()

    max_height = int(min(max_height*up_scale, frames[0].shape[0]))
    max_width = int(min(max_width*up_scale, frames[0].shape[1]))

    max_height = max(max_height, max_width)
    max_width = max(max_height, max_width)

    center_height = int((locations[:, bottom_index].mean() + locations[:, top_index].mean()) / 2)
    center_width = int((locations[:, left_index].mean() + locations[:, right_index].mean()) / 2)

    top = int(center_height - max_height / 2)
    right = int(center_width + max_width / 2)
    bottom = int(center_height + max_height / 2)
    left = int(center_width - max_width / 2)

    if top < 0 or left < 0 or bottom >= frames[0].shape[0] or right >= frames[0].shape[1]:
        return cut_section(frames, locations, start_frame, up_scale - over_size_scale_steps, buffer_size,
                           over_size_scale_steps)

    new_frames = list()

    for frame in frames[start_frame:]:
        section = frame[top:bottom, left:right]
        new_frames.append(section)

        # print(section.shape)
        # cv2.imshow("test", section)
        # cv2.waitKey(10)

    return new_frames


def cut_singular_moving_faces_from_group(scenes, watch_interval=5, image_scale=2, movement_threshold_image=1, movement_threshold_video=0.8, **kwargs):
    new_list = list()

    for frames in scenes:

        pos_list, spawn_index = identify_all_faces(frames, watch_interval, image_scale)

        singular_moving_face = None

        for i in range(len(pos_list)):

            section_list = cut_section(frames, pos_list[i], spawn_index[i], **kwargs)
            past_frame = None
            movement_count = 0

            for c, section in enumerate(section_list):
                if c % watch_interval != 0:
                    continue

                gray = cv2.cvtColor(section, cv2.COLOR_BGR2GRAY)
                if past_frame is not None:
                    change = cv2.absdiff(gray, past_frame)
                    # cv2.imshow("change", change)
                    # cv2.waitKey(20)
                    if change.mean() >= movement_threshold_image:
                        movement_count += 1

                past_frame = gray

            # print(movement_count * watch_interval / len(section_list))
            if movement_count * watch_interval / len(section_list) >= movement_threshold_video:

                if singular_moving_face is not None:
                    singular_moving_face = None
                    break
                else:
                    singular_moving_face = section_list

        if singular_moving_face is not None:
            new_list.append(singular_moving_face)

    return new_list

def run(config_path, target, conn):
    with open(config_path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

    my_key = "moving_face_isolator"
    if my_key not in data:
        print("key (" + my_key + " ) is missing in " + config_path)
        return -1

    if not os.path.exists(target):
        print("dir: " + str(target) + " not found")

    data = data[my_key]
    worker_count = data["sub_process_count"] if "sub_process_count" in data else 1
    watch_interval = data["watch_interval"] if "watch_interval" in data else 5
    image_scale = data["image_scale"] if "image_scale" in data else 2
    movement_threshold_image = data["movement_threshold_image"] if "movement_threshold_image" in data else 1
    movement_threshold_video = data["movement_threshold_video"] if "movement_threshold_video" in data else 0.8



if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("need exactly 2 args (config-path) and (taget_file)")

    config_path = sys.argv[1]
    target_file = sys.argv[2]

    run(config_path, target_file, None)