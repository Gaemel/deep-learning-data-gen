import cv2
import numpy as np
import os
from copy import deepcopy


def change_res(cap, width, height):
    cap.set(3, width)
    cap.set(4, height)

STD_DIMENSIONS = {
    "bob": (360, 640),
    "480p": (640, 480),
    "720p": (1280, 720),
    "1080p": (1920, 1080),
    "4k": (3840, 2160)
}

def get_dims(cap, res='1080p'):
    width, height = STD_DIMENSIONS['480p']
    if res in STD_DIMENSIONS:
        width, height = STD_DIMENSIONS[res]
    change_res(cap, width, height)
    return width, height

VIDEO_TYPE = {
    'avi': cv2.VideoWriter_fourcc(*"XVID"),
    # 'mp4': cv2.VideoWriter_fourcc(*"XVID"),
    'mp4': cv2.VideoWriter_fourcc(*"H264")
}

def get_video_type(filename):
    filename, ext = os.path.splitext(filename)
    if ext in VIDEO_TYPE:
        return VIDEO_TYPE[ext]
    return VIDEO_TYPE['avi']

cap = cv2.VideoCapture(0)

filename_cap = "vid_out/cam_test.mp4"
filename_vid_cap = "vid_out/video_test.mp4"
frames_per_seconds = 25.0
res = 'bob'
vid_cap = cv2.VideoCapture('tagesschau_000.mp4')

fourcc = cv2.VideoWriter_fourcc(*"mp4v")

dims = (200, 200)

out_list = list()
out = cv2.VideoWriter(filename_cap, fourcc, frames_per_seconds, dims)
out_vid = cv2.VideoWriter(filename_vid_cap, fourcc, frames_per_seconds, dims)
# out = cv2.VideoWriter(filename, fourcc=video_type_cv2, fpd=frames_per_seconds, frameSize=dims)

i = 0
while True:

    success, frame = cap.read()
    success_vid, frame_vid = vid_cap.read()

    frame = cv2.resize(frame, dims)
    frame_vid = cv2.resize(frame_vid, dims)

    out.write(frame)
    out_vid.write(frame_vid)
    cv2.imshow("frame", frame)
    cv2.imshow("frame_vid", frame_vid)

    i += 1
    if cv2.waitKey(20) & 0xFF == ord('q'):
        break
    if not success or not success_vid:
        break

out.release()
cap.release()
cv2.destroyAllWindows()