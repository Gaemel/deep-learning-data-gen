import os
import cv2

from commen import save

def get_all_txt_in_dir(main_dir):

    content = list()
    for sub in os.listdir(main_dir):
        temp_path = os.path.join(main_dir, sub)
        if os.path.isdir(temp_path):
            temp = get_all_txt_in_dir(temp_path)
            if isinstance(temp, list):
                content += temp
        elif os.path.splitext(temp_path)[-1] == ".txt":
            content.append(temp_path)
    return content

def create_pre_train_data(main_dir, out_dir):
    txt_list = get_all_txt_in_dir(main_dir)
    for i, t in enumerate(txt_list):
        cut_video(t, os.path.join(out_dir, str(i)))

def load_video(video_path):
    cap = cv2.VideoCapture(video_path)
    fps = cap.get(cv2.CAP_PROP_FPS)

    frames = list()
    while True:
        success, frame = cap.read()
        if not success:
            break
        frames.append(frame)

    return frames, fps

def cut_video(txt_path, out_dir):
    temp = os.path.splitext(txt_path)[0]
    video_path = temp + ".mp4"
    temp = os.path.split(temp)[-1]
    video, fps = load_video(video_path)

    meta = load_meta_file_as_list(txt_path)
    i = 0
    for text, start_time, end_time in meta:
        name = temp + "_" + str(i)
        start_index = int(float(start_time) * fps)
        end_index = min(int(float(end_time) * fps) + 1, len(video)-1)

        if start_index - 2 >= 0:
            start_index -= 2
        elif start_index -1 >= 0:
            start_index -= 1

        if end_index + 2 < len(video):
            end_index += 2
        elif end_index +1 < len(video):
            end_index += 1

        clip = video[start_index:end_index]
        meta_file = {"start": start_index, "end": end_index, "video_fps": fps, "frames": end_index - start_index}
        text = "Text: " + text + "\n" + "Conf: 10"
        save(out_dir, name, meta=meta_file, text=text, video=clip, verbose=True)
        i += 1

def load_meta_file_as_list(file_path):
    if not os.path.exists(file_path):
        return None

    content = list()
    start_found = False
    with open(file_path, "r") as file:
        for x in range(50):
            line = file.readline().replace("\n", "")
            if start_found:
                if line == "":
                    break
                else:
                    content.append(line.split(" ")[:3])
            elif line == "WORD START END ASDSCORE":
                start_found = True

    return content

if __name__ == '__main__':
    file_path = """/media/ubuntu/data/data/2/mvlrs_v1/pretrain/"""
    out_dir = """/media/ubuntu/data/combined/pre_train"""
    create_pre_train_data(file_path, out_dir)