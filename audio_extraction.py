import moviepy.editor
import sys
import os
from multiprocessing import Pool
from functools import partial
import shutil
from audio_test import speech_to_text

import commen

#
# # Replace the parameter with the location of the video
# video = moviepy.editor.VideoFileClip('tagesschau_000.mp4')
#
# video = video.subclip(20, 25.75)
#
# audio = video.audio
#
# # Replace the parameter with the location along with filename
# audio.write_audiofile('tagesschau_000.wav')
# video.write_videofile("test_000.mp4")


def audio_sequence_to_text(meta_file, output_dir, config_path, config_key):
    # print("start on: " + str(meta_file))
    meta_data = commen.load_config(meta_file)
    config = commen.load_config(config_path, config_key)
    fps = meta_data[commen.KEY_VIDEO_FPS] if commen.KEY_VIDEO_FPS else 25

    start = meta_data[commen.KEY_START]
    end = meta_data[commen.KEY_END]

    if start > end:
        print(meta_file)
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    video_path = os.path.join(output_dir, os.path.split(meta_data[commen.KEY_VIDEO])[-1])

    audio_path = str(os.path.splitext(video_path)[0]) + ".wav"

    og_vid_path = os.path.join(output_dir, os.path.split(meta_data[commen.KEY_ORIGINAL_VIDEO])[-1])
    video = moviepy.editor.VideoFileClip(og_vid_path, verbose=False)

    video = video.subclip(start/fps, end/fps)

    audio = video.audio

    meta_file_name, _ = os.path.splitext(os.path.split(meta_file)[-1])
    new_loc = commen.copy(meta_file, output_dir, False)

    new_meta_data_path = os.path.join(new_loc, os.path.split(meta_file)[-1])

    meta_data = commen.load_config(new_meta_data_path)
    meta_data[commen.KEY_AUDIO] = audio_path
    commen.save(output_dir, meta_file_name, meta=meta_data, audio=audio)
    temp = os.path.join(output_dir, meta_file_name + "_cut.mp4")
    video.write_videofile(temp)

def run(config_path, target):
    my_key = "audio_extractor"
    data = commen.load_config(config_path, my_key)

    remove_old = data[commen.KEY_REMOVE_OLD] if commen.KEY_REMOVE_OLD in data else False
    worker_count = data["sub_process_count"] if "sub_process_count" in data else 1

    if not os.path.exists(target):
        print("(audio) target does not exist: " + str(target))
        return

    meta_files = [os.path.join(target, x) for x in os.listdir(target) if str(x).endswith(".yaml")]

    if len(meta_files) >= 1:

        og_video = commen.load_config(meta_files[0])[commen.KEY_ORIGINAL_VIDEO]
        if not os.path.exists(og_video):
            print("(audio) og_video does not exist: " + str(og_video))
            return

        out_dir = os.path.join(data["output_dir"], os.path.splitext(os.path.split(og_video)[-1])[0])
        new_og_video = os.path.join(out_dir, os.path.split(og_video)[-1])
        os.makedirs(out_dir, exist_ok=True)

        if not os.path.exists(new_og_video):
            shutil.copy(og_video, new_og_video)

        astt_with_args = partial(audio_sequence_to_text, output_dir=out_dir, config_path=config_path,
                                 config_key=my_key)
        with Pool(processes=worker_count) as pool:
            pool.map(astt_with_args, meta_files)
    else:
        print("(audio) meta_files do not exist: " + str(target))

    if remove_old:
        try:
            shutil.rmtree(target)
        except:
            pass


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("need exactly 2 args (config-path) and (taget_file)")

    config_path = sys.argv[1]
    target_file = sys.argv[2]

    run(config_path, target_file)


