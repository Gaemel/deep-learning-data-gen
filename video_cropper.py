import cv2
from clear_text import collect_files
import os
from shutil import copy
import time
import threading

def crop_video(source, target, border):
    cap = cv2.VideoCapture(source)
    fps = cap.get(cv2.CAP_PROP_FPS)
    scene = list()

    while True:
        s, f = cap.read()
        # print()
        if not s:
            break

        f = f[border:-border, border:-border, :]
        scene.append(f)

    cap.release()

    fourcc = cv2.VideoWriter_fourcc(*"mp4v")
    shape = scene[0].shape[:-1]
    out = cv2.VideoWriter(target, fourcc, fps, shape)

    for frame in scene:
        out.write(frame)

    out.release()


def copy_other_file(file, out_file, file_ending):
    temp_file = os.path.splitext(file)[0] + file_ending
    if not os.path.exists(temp_file):
        return

    temp_out_file = os.path.splitext(out_file)[0] + file_ending
    copy(temp_file, temp_out_file)

def crop_dir(border, target_dir, out_dir=None, copy_other_files=None):
    updater_step = 1000
    counter = 0
    for file in collect_files(target_dir, ".mp4"):
        if out_dir is None:
            out_file = file
        else:
            out_file = os.path.join(out_dir, os.path.split(file)[-1])

        crop_video(file, out_file, border)
        if copy_other_files is not None:
            for fe in copy_other_files:
                copy_other_file(file, out_file, fe)

        counter += 1
        if counter % updater_step == 0:
            print(counter)


def work(border, targets, out_dir=None, copy_other_files=None):
    for file in targets:
        if out_dir is None:
            out_file = file
        else:
            out_file = os.path.join(out_dir, os.path.split(file)[-1])

        crop_video(file, out_file, border)
        if copy_other_files is not None:
            for fe in copy_other_files:
                copy_other_file(file, out_file, fe)

def pop_list(target, batch_size):
    if len(target) <= batch_size:
        return target, []
    else:
        batch = target[:batch_size]
        target = target[batch_size:]

    return batch, target

def run_threading(n_threads, batch_size, border, target_dir, out_dir=None, copy_other_files=None):
    targets = collect_files(target_dir, ".mp4")

    next_batch = None
    threads = [None] * n_threads
    i = 0

    while True:
        change = False
        if next_batch is None:
            next_batch, targets = pop_list(targets, batch_size)
        if len(next_batch) == 0:
            break

        for x in range(len(threads)):
            if threads[x] is None or not threads[x].is_alive():
                threads[x] = threading.Thread(target=work, args=(border, next_batch, out_dir, copy_other_files))
                threads[x].start()
                next_batch = None
                change = True
                print("started thread: " + str(i))
                i += 1
                break

        if not change:
            time.sleep(5)

    for t in threads:
        if t is not None:
            t.join()

    print("done")


def test():
    some_list = list(range(100))
    a, some_list = pop_list(some_list, 10)
    print()

def crop():
    source = """/media/ubuntu/data/combined/pre_train"""
    out = """/media/ubuntu/data/combined/120/pre_train"""
    border = 30

    crop_dir(border, source, out, [".txt"])

    print("done")

def crop_threading():
    source = """/media/ubuntu/data/combined/english"""
    out = """/media/ubuntu/data/combined/100/english"""
    border = 30
    n_threads = 32
    bs = 1000

    run_threading(n_threads, bs, border, source, out, [".txt"])

    print("done")


if __name__ == '__main__':
    crop_threading()
