import os
from clear_text import collect_files
from shutil import copy

def get_all_files(parent_dir):

    file_list = list()

    file_list_mp4 = collect_files(parent_dir, ".mp4")
    file_list_txt = collect_files(parent_dir, ".txt")

    for mp4 in file_list_mp4:
        temp = os.path.splitext(mp4)[0] + ".txt"
        if temp in file_list_txt:
            file_list_txt.remove(temp)
            file_list.append(temp)
            file_list.append(mp4)
        else:
            print("drop: ", temp)

    return file_list

def ensure_dir(some_file):
    if some_file.endswith(".txt") or some_file.endswith(".mp4"):
        some_file = os.path.split(some_file)[0]

    os.makedirs(some_file, exist_ok=True)

if __name__ == '__main__':
    source_dir = """/media/ubuntu/data/lip_reading_data/da_german"""
    out_dir = """/media/ubuntu/data/lip_reading_data/german_lip_reading_dataset"""

    all_files = get_all_files(source_dir)

    for f in all_files:
        f_target = os.path.join(out_dir, f[len(source_dir)+1:])
        # print(out_dir)
        ensure_dir(f_target)
        copy(f, f_target)



