import cv2
import os


def collect_mp4_files(parent_dir):
    res_list = list()
    for title in os.listdir(parent_dir):
        sub_dir = os.path.join(parent_dir, title)
        if os.path.isdir(sub_dir):
            res_list += collect_mp4_files(sub_dir)
        elif sub_dir.endswith(".mp4"):
            res_list.append(sub_dir)
    return res_list


def convert_scene(target_path):
    cap = cv2.VideoCapture(target_path)
    scene = list()

    while True:
        s, f = cap.read()
        if not s:
            break
        scene.append(cv2.cvtColor(f, cv2.COLOR_BGR2GRAY))
        scene.append(f)
    cap.release()

    ext = ".mp4"
    fourcc = cv2.VideoWriter_fourcc(*"mp4v")

    out = cv2.VideoWriter(target_path, fourcc, 25, (160, 160), 0)

    for frame in scene:
        # cv2.imshow('Black white image', frame)
        # cv2.waitKey()
        out.write(frame)

    out.release()


def make_all_bw(target_dir):
    paths = collect_mp4_files(target_dir)

    for p in paths:
        print(p)
        convert_scene(p)

    print("done")

if __name__ == '__main__':
    target_dir = """/media/ubuntu/data/data/2/mvlrs_v1"""
    make_all_bw(target_dir)