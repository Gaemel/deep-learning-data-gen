import cv2
import numpy as np
import os
import yaml
from bb import BB
from copy import copy
from clear_text import collect_files

import shutil

class DetectorDataGenerator(BB):

    def __init__(self, out_dir, result_size, target_size, watch_interval=5, file_list=None):
        super().__init__(out_dir, target_size, watch_interval, file_list)
        self._result_size = result_size
        self._crop_img = None
        self._crop_points = list()

    def show(self):
        self._shown_img, self._ration = BB.scale_to(self._target_img, self._target_size)
        self.update_show_img()

    def click_event(self, event, x, y, flags, params):
        if event == cv2.EVENT_MOUSEMOVE:
            if len(self._points) > 0:
                temp_point = None

                while len(self._points) > 1:
                    temp_point = self._points.pop(-1)

                x_offset = x - self._points[0][0]
                y_offset = y - self._points[0][1]

                if abs(x_offset) > abs(y_offset):
                    offset = x_offset
                else:
                    offset = y_offset

                if x_offset > 0:
                    x_offset = abs(offset)
                else:
                    x_offset = -abs(offset)

                if y_offset > 0:
                    y_offset = abs(offset)
                else:
                    y_offset = -abs(offset)

                if 0 <= self._points[0][0] + x_offset <= self._target_img.shape[1] and 0 <= self._points[0][1] + y_offset <= self._target_img.shape[0]:
                    x = self._points[0][0] + x_offset
                    y = self._points[0][1] + y_offset

                    new_pos = np.array((x, y))
                    self._points.append(new_pos)
                elif temp_point is not None:
                    self._points.append(temp_point)

            self.update_show_img()

        elif event == cv2.EVENT_LBUTTONDOWN:
            if len(self._points) == 0:
                new_pos = np.array((x, y))
                self._points.append(new_pos)
            else:
                self._crop_points = copy(self._points)
                self.draw_preview()

            self.update_show_img()

    def get_crop_pt(self, as_dict=False):
        if len(self._crop_points) != 2:
            return None

        x_min = min(self._crop_points[0][0], self._crop_points[1][0])
        y_min = min(self._crop_points[0][1], self._crop_points[1][1])
        x_max = max(self._crop_points[0][0], self._crop_points[1][0])
        y_max = max(self._crop_points[0][1], self._crop_points[1][1])

        if as_dict:
            return {self.PT_0: {"x": int(x_min), "y": int(y_min)}, self.PT_1: {"x": int(x_max), "y": int(y_max)}}
        else:
            return x_min, y_min, x_max, y_max

    def quit(self):
        self._end = True
        cv2.destroyAllWindows()

    def update_show_img(self):
        skip = True
        self.draw()
        cv2.imshow("img", self._shown_img)
        if not self._event_set:
            self._event_set = True
            cv2.setMouseCallback('img', self.click_event)
            while skip:
                temp = cv2.waitKey()

                if chr(temp) == "s":
                    skip = False
                    self.save()
                elif chr(temp) == "n":
                    skip = False
                    self.next()
                elif chr(temp) == "q":
                    skip = False
                    self.quit()
                elif chr(temp) == "r":
                    self._points = list()

    def next(self):
        self._event_set = False
        self._crop_img = None
        self._points = list()
        self._crop_points = list()

    def save(self):
        print("saving...")

        if self._crop_img is None:
            print("no crop")
            self.next()
            return

        os.makedirs(self._out_dir, exist_ok=True)
        ok = False
        while not ok:

            temp_title = self._target_title + "_" + str(self._title_index) + ".png"
            temp_title = os.path.join(self._out_dir, temp_title)
            if os.path.exists(temp_title):
                self._title_index += 1
            else:
                ok = True


        cv2.imwrite(temp_title,  cv2.resize(self._crop_img, self._result_size))
        print("to: " + str(temp_title))
        self._title_index += 1

        self.next()

    def draw(self):
        temp = self.get_pt()

        if temp is None:
            return

        x_min, y_min, x_max, y_max = temp

        x_min = int(x_min * self._ration)
        y_min = int(y_min * self._ration)
        x_max = int(x_max * self._ration)
        y_max = int(y_max * self._ration)

        self._shown_img, self._ration = BB.scale_to(copy(self._target_img), self._target_size)
        self._shown_img = cv2.rectangle(self._shown_img, (x_min, y_min), (x_max, y_max), (0, 255, 0), 1)
        # self.update_show_img()

        temp = self.get_crop_pt()

        if temp is None:
            return

        x_min, y_min, x_max, y_max = temp

        x_min = int(x_min * self._ration)
        y_min = int(y_min * self._ration)
        x_max = int(x_max * self._ration)
        y_max = int(y_max * self._ration)

        self._shown_img = cv2.rectangle(self._shown_img, (x_min, y_min), (x_max, y_max), (0, 0, 255), 1)

    def draw_preview(self):
        temp = self.get_pt()

        if temp is None:
            return

        x_min, y_min, x_max, y_max = temp

        x_min = int(x_min * self._ration)
        y_min = int(y_min * self._ration)
        x_max = int(x_max * self._ration)
        y_max = int(y_max * self._ration)

        self._crop_img = copy(self._target_img)[y_min:y_max, x_min:x_max]
        cv2.imshow("crop", self._crop_img)


class Classifacation():

    def __init__(self, input_dir, out_dir, options):
        self._options = options
        self._input_dir = input_dir
        self._output_dir = out_dir
        self._target_list = list()
        self._class_dict = dict()
        self._index = 0
        self._end = False

    def save(self):
        print("saving stuff")
        for o in self._options:
            os.makedirs(os.path.join(self._output_dir, o), exist_ok=True)

        for i, f in enumerate(self._target_list):

            if i not in self._class_dict or self._class_dict[i] is None:
                continue
            print("saving: " + str(os.path.join(self._output_dir, self._class_dict[i], f)))
            shutil.copy(os.path.join(self._input_dir, f), os.path.join(self._output_dir, self._class_dict[i], f))

    def prepare_target_list(self):
        self._target_list = list()
        for f in os.listdir(self._input_dir):
            if str(f).endswith(".png"):
                self._target_list.append(f)
        for o in self._options:
            temp_dir = os.path.join(self._output_dir, o)
            if os.path.exists(temp_dir):
                for f in os.listdir(temp_dir):
                    if f in self._target_list:
                        print("remove " + str(f))
                        self._target_list.remove(f)

    def classify(self, label):
        print("classified " + str(self._index) + " of " + str(len(self._target_list)))
        self._class_dict[self._index] = label

    def back(self):
        self._index = max(self._index - 1, 0)
        self._class_dict[self._index] = None

    def show_img(self, i):
        cv2.imshow("img", cv2.imread(os.path.join(self._input_dir, self._target_list[i])))

    def is_done(self):
        return not self._end and self._index >= len(self._target_list)

    def next(self):
        done = False
        while not done:
            self.show_img(self._index)
            key = chr(cv2.waitKey())
            if key == "q":
                self._end = True
                self.save()
                done = True

            elif key == "x":
                done = True
                self._index += 1

            elif key == "r":
                self.back()
                done = True

            elif key in self._options:

                self.classify(key)
                self._index += 1
                done = True
            else:
                print("wrong key")

    def run(self):
        self.prepare_target_list()
        while not self.is_done():
            self.next()
        cv2.destroyAllWindows()
        self.save()


def resize(input_dir, output_dir, target_size):
    for f in os.listdir(input_dir):
        if str(f).endswith(".png"):
            img = cv2.resize(cv2.imread(os.path.join(input_dir, f)), target_size)
            cv2.imwrite(os.path.join(output_dir, f), img)
    print("done")


def do_resize():
    in_dir = """/media/ubuntu/data/lip_reading_data/debug_1/0"""
    out_dir = """/media/ubuntu/data/lip_reading_data/class_train/0"""
    os.makedirs(out_dir, exist_ok=True)
    target_size = (160, 160)

    resize(in_dir, out_dir, target_size)


def classify():
    options = ["1", "0"]
    in_dir = """/media/ubuntu/data/lip_reading_data/debug"""
    out_dir = """/media/ubuntu/data/lip_reading_data/debug_1"""
    c = Classifacation(input_dir=in_dir, out_dir=out_dir, options=options)
    c.run()


def cropper():
    main_dir = """/media/ubuntu/data/lip_reading_data/da_german"""
    files = collect_files(main_dir, ".mp4")
    out_dir = """/media/ubuntu/data/lip_reading_data/debug"""
    r_size = (160, 160)
    ddg = DetectorDataGenerator(out_dir, r_size, None, watch_interval=20, file_list=files)
    ddg.run()

if __name__ == '__main__':
    cropper()
