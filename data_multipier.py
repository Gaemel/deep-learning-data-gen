import os
import cv2
import yaml
from bb import BB
from copy import copy
from random import randint, shuffle, choices


def load_annotations(target_path):
    with open(target_path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data


def load_img(target_path, target_shape):
    temp = cv2.imread(target_path)
    if temp.shape[0] != target_shape[0] or temp.shape[1] != target_shape[1]:
        return None
    else:
        return temp

def crop_all_sides(img, bb_point_0, bb_point_1):
    # show_with_bb(img, bb_point_0, bb_point_1, title="ori")

    x_min, y_min = bb_point_0
    x_max, y_max = bb_point_1

    x_size = img.shape[0]
    y_size = img.shape[1]

    # print(bb_point_0)
    # print(bb_point_1)

    crop_0 = min(x_min, y_min)
    crop_1 = min(x_size - x_max, y_min)
    crop_2 = min(x_size - x_max, y_size - y_max)
    crop_3 = min(x_size - x_max, y_size - y_max)

    crop_4 = min(int(min(x_min, x_size - x_max) * 2), y_min)
    crop_5 = min(x_min, min(y_min, y_size - y_max) * 2)
    crop_6 = min(x_size - x_max, min(y_min, y_size-y_max))
    crop_7 = min(min(x_min, x_size - x_max), y_size - y_min)

    crop_list = list()

    try:
        crop_list.append(crop(img, bb_point_0, bb_point_1, crop_0, 0))
        crop_list.append(crop(img, bb_point_0, bb_point_1, crop_1, 1))
        crop_list.append(crop(img, bb_point_0, bb_point_1, crop_2, 2))
        crop_list.append(crop(img, bb_point_0, bb_point_1, crop_3, 3))
        crop_list.append(crop(img, bb_point_0, bb_point_1, crop_4, 4))
        crop_list.append(crop(img, bb_point_0, bb_point_1, crop_5, 5))
        crop_list.append(crop(img, bb_point_0, bb_point_1, crop_6, 6))
        crop_list.append(crop(img, bb_point_0, bb_point_1, crop_7, 7))
    except:
        pass

    return crop_list


def scape_point(point, ratio):
    return (int(point[0] * ratio), int(point[1] * ratio))


def bound_point(point, max_value):
    return (max(0, min(max_value, point[0])), max(0, min(max_value, point[1])))


def crop(img, bb_0, bb_1, max_range, mode):
    value = randint(0, max_range)
    # value = max_range

    if mode == 0:
        crop_img = img[value:, value:]

        ratio = img.shape[0] / crop_img.shape[0]

        crop_bb_0 = (bb_0[0] - value, bb_0[1] - value)
        crop_bb_1 = (bb_1[0] - value, bb_1[1] - value)

        crop_bb_0 = scape_point(crop_bb_0, ratio)
        crop_bb_1 = scape_point(crop_bb_1, ratio)

        crop_bb_0 = bound_point(crop_bb_0, img.shape[0])
        crop_bb_1 = bound_point(crop_bb_1, img.shape[1])

        # show_with_bb(crop_img, crop_bb_0, crop_bb_1, "crop_0")

    if mode == 1:
        crop_img = img[value:, : -value]

        ratio = img.shape[0] / crop_img.shape[0]

        crop_bb_0 = (bb_0[0], bb_0[1] - value)
        crop_bb_1 = (bb_1[0], bb_1[1] - value)

        crop_bb_0 = scape_point(crop_bb_0, ratio)
        crop_bb_1 = scape_point(crop_bb_1, ratio)

        crop_bb_0 = bound_point(crop_bb_0, img.shape[0])
        crop_bb_1 = bound_point(crop_bb_1, img.shape[1])

        # show_with_bb(crop_img, crop_bb_0, crop_bb_1, "crop_1")

    if mode == 2:
        crop_img = img[: -value, : -value]

        ratio = img.shape[0] / crop_img.shape[0]

        crop_bb_0 = (bb_0[0], bb_0[1])
        crop_bb_1 = (bb_1[0], bb_1[1])

        crop_bb_0 = scape_point(crop_bb_0, ratio)
        crop_bb_1 = scape_point(crop_bb_1, ratio)

        crop_bb_0 = bound_point(crop_bb_0, img.shape[0])
        crop_bb_1 = bound_point(crop_bb_1, img.shape[1])

        # show_with_bb(crop_img, crop_bb_0, crop_bb_1, "crop_2")

    if mode == 3:
        crop_img = img[: -value, value:]

        ratio = img.shape[0] / crop_img.shape[0]

        crop_bb_0 = (bb_0[0] - value, bb_0[1])
        crop_bb_1 = (bb_1[0] - value, bb_1[1])

        crop_bb_0 = scape_point(crop_bb_0, ratio)
        crop_bb_1 = scape_point(crop_bb_1, ratio)

        crop_bb_0 = bound_point(crop_bb_0, img.shape[0])
        crop_bb_1 = bound_point(crop_bb_1, img.shape[1])


        # show_with_bb(crop_img, crop_bb_0, crop_bb_1, "crop_3")

        # cv2.imshow("crop", crop_img)
        # cv2.waitKey()

    if mode == 4:
        crop_img = img[value:, int(value / 2): - int(value / 2)]

        ratio = img.shape[0] / crop_img.shape[0]

        crop_bb_0 = (int((bb_0[0] - value / 2) * ratio), int((bb_0[1] - value) * ratio))
        crop_bb_1 = (int((bb_1[0] - value / 2) * ratio), int((bb_1[1] - value) * ratio))


        # show_with_bb(crop_img, crop_bb_0, crop_bb_1, "crop_4")

    if mode == 5:
        crop_img = img[int(value / 2): - int(value / 2):, value:]

        ratio = img.shape[0] / crop_img.shape[0]

        crop_bb_0 = (int((bb_0[0] - value) * ratio), int((bb_0[1] - value / 2) * ratio))
        crop_bb_1 = (int((bb_1[0] - value) * ratio), int((bb_1[1] - value / 2) * ratio))

        # show_with_bb(crop_img, crop_bb_0, crop_bb_1, "crop_5")

    if mode == 6:
        crop_img = img[:-value, int(value / 2): - int(value / 2)]

        ratio = img.shape[0] / crop_img.shape[0]

        crop_bb_0 = (int((bb_0[0] - value / 2) * ratio), int((bb_0[1]) * ratio))
        crop_bb_1 = (int((bb_1[0] - value / 2) * ratio), int((bb_1[1]) * ratio))

        # show_with_bb(crop_img, crop_bb_0, crop_bb_1, "crop_6")

    if mode == 7:
        crop_img = img[int(value / 2): - int(value / 2):, :-value]

        ratio = img.shape[0] / crop_img.shape[0]

        crop_bb_0 = (int((bb_0[0]) * ratio), int((bb_0[1] - value / 2) * ratio))
        crop_bb_1 = (int((bb_1[0]) * ratio), int((bb_1[1] - value / 2) * ratio))


    crop_img = cv2.resize(crop_img, (img.shape[0], img.shape[1]))

    return crop_img, crop_bb_0, crop_bb_1


def show_with_bb(img, bb_point_0, bb_point_1, title="img", delay=None):
    bb_img = copy(img)
    bb_img = cv2.rectangle(bb_img, bb_point_0, bb_point_1, (255, 0, 0), 2)
    cv2.imshow(title, bb_img)

    cv2.waitKey(delay)


def rotate(img, bb_point_0, bb_point_1, include_original=True, flip=True):
    res_list = list()
    bb_point_0_list = list()
    bb_point_1_list = list()

    if include_original:
        res_list.append(img)
        bb_point_0_list.append(bb_point_0)
        bb_point_1_list.append(bb_point_1)

    x = img.shape[0]
    y = img.shape[1]

    # vertical
    temp = cv2.flip(img, 1)

    temp_bb_0 = (x - bb_point_1[0], bb_point_0[1])
    temp_bb_1 = (x - bb_point_0[0], bb_point_1[1])

    res_list.append(temp)
    bb_point_0_list.append(temp_bb_0)
    bb_point_1_list.append(temp_bb_1)

    # horizontal
    if flip:
        temp = cv2.flip(img, 0)

        temp_bb_0 = (bb_point_0[0], y - bb_point_1[1])
        temp_bb_1 = (bb_point_1[0], y - bb_point_0[1])

        res_list.append(temp)
        bb_point_0_list.append(temp_bb_0)
        bb_point_1_list.append(temp_bb_1)

        # horizontal and vertical

        temp = cv2.flip(cv2.flip(img, 1), 0)

        bb_point_0 = temp_bb_0
        bb_point_1 = temp_bb_1

        temp_bb_0 = (x - bb_point_1[0], bb_point_0[1])
        temp_bb_1 = (x - bb_point_0[0], bb_point_1[1])

        temp_bb = temp_bb_0

        res_list.append(temp)
        bb_point_0_list.append(temp_bb_0)
        bb_point_1_list.append(temp_bb_1)

        # for i in range(len(res_list)):
        #     show_with_bb(res_list[i], bb_point_0_list[i], bb_point_1_list[i], "img_" + str(i))
        # cv2.waitKey()

    return res_list, bb_point_0_list, bb_point_1_list


def load_points(annotations, key):
    return (annotations[key][BB.PT_0]["x"], annotations[key][BB.PT_0]["y"]), (annotations[key][BB.PT_1]["x"], annotations[key][BB.PT_1]["y"])


def save_img(target_dir, img, bb_0, bb_1, annotations, og_title, padding):
    if img.shape[0] != 160 or img.shape[1] != 160:
        print(img.shape)
        cv2.imshow("???", img)
        cv2.waitKey()
    new_key = str(os.path.splitext(og_title)[0]) + "_" + padding + str(os.path.splitext(og_title)[1])
    new_title = os.path.join(target_dir, new_key)
    print("save: " + str(new_title))
    cv2.imwrite(new_title, img)

    points = {BB.PT_0: {"x": bb_0[0], "y": bb_0[1]}, BB.PT_1: {"x": bb_1[0], "y": bb_1[1]}}

    annotations[new_key] = points


def show_all(target_shape):
    target_dir = """/media/ubuntu/data/lip_reading_data/test_data"""

    annotations = load_annotations(os.path.join(target_dir, "annotations.yaml"))

    keys = list(annotations.keys())
    shuffle(keys)
    for key in keys:
        img = load_img(os.path.join(target_dir, key), target_shape)
        p_0, p_1 = load_points(annotations, key)

        show_with_bb(img, p_0, p_1, delay=75)
        if img.shape[0] != 160 or img.shape[1] != 160:
            print(img.shape)
            cv2.waitKey()


def main(multiplication_value=None, flip=True, target_shape=(160, 160)):
    target_dir = """/media/ubuntu/data/lip_reading_data/test_data_raw"""
    out_dir = """/media/ubuntu/data/lip_reading_data/test_data"""

    os.makedirs(out_dir, exist_ok=True)

    annotations = load_annotations(os.path.join(target_dir, "annotations.yaml"))
    new_annotations = dict()

    for key in annotations:

        img = load_img(os.path.join(target_dir, key), target_shape)
        if img is None:
            continue

        p_0, p_1 = load_points(annotations, key)


        img_list, p_0_list, p_1_list = rotate(img, p_0, p_1, include_original=True, flip=flip)

        res_list = list()

        for i in range(len(img_list)):
            temp_img, temp_p_0, temp_p_1 = img_list[i], p_0_list[i], p_1_list[i]
            save_img(out_dir, temp_img, temp_p_0, temp_p_1, new_annotations, key, "_" + str(i))

            # content = (temp_img, temp_p_0, temp_p_1)
            # res_list.append(content)
            # save_img(out_dir, temp_img, temp_p_0, temp_p_1, new_annotations, key, "_" + str(i))

            #
            # for content in crop_all_sides(temp_img, temp_p_0, temp_p_1):
            #     res_list.append(content)
            #ann
            # if multiplication_value is None:
            #     selection = res_list
            # else:
            #     selection = choices(res_list, k=multiplication_value)
            #
            # for j in range(len(selection)):
            #     crop_img, crop_p_0, crop_p_1 = selection[j]
            #     save_img(out_dir, crop_img, crop_p_0, crop_p_1, new_annotations, key, "_" + str(i) + "_" + str(j))

    with open(os.path.join(out_dir, "annotations.yaml"), "w") as file:
        yaml.dump(new_annotations, file)


if __name__ == '__main__':
    target_shape = (160, 160)

    main(2, flip=False, target_shape=target_shape)
    show_all(target_shape)
