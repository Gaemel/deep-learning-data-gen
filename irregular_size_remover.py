import cv2
from make_bw import collect_mp4_files
import os

def get_shape(file):
    cap = cv2.VideoCapture(file)
    s, f = cap.read()
    if not s:
        return None
    f = cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
    shape = f.shape
    cap.release()
    return shape

def remove(parent_dir, shape_white_list):
    mp4_list = collect_mp4_files(parent_dir)
    i = 0
    for file in mp4_list:
        if not get_shape(file) in shape_white_list:
            i += 1
            os.remove(file)
            print(file)

    print("removed: ", str(i))
    print("done")

if __name__ == '__main__':
    target_dir = """/media/ubuntu/data/lip_reading_data/text"""
    white_list = [(160, 160)]
    remove(target_dir, white_list)