import time
import os
from multiprocessing import Process, Pipe
import yaml
import signal
import sys
import random

from scene_splitter import run as scene_splitter_run
from face_splitter import run as face_splitter_run
from slicer import run as slicer_run
from audio_extraction import run as audio_extractor_run
from text_creator import run as text_creator_run
from downloader import run as download_run

import datetime

import commen

class ToolManager():

    DOWNLOADER = "downloader"
    SCENE_SPLITTER = "scene_splitter"
    FACE_SPLITTER = "face_splitter"
    FACE_SLICER = "face_slicer"
    # GROUP_SLICER = "group_slicer"
    AUDIO_EXTRACTOR = "audio_extractor"
    TEXT_CREATOR = "text_creator"
    DONE = "done"

    PROCESS_COUNT = "process_count"

    def __init__(self, queued=False):
        self.queued = queued

        self.config_path = "./config.yaml"

        config = commen.load_config(self.config_path)
        self.target_file = config[self.DOWNLOADER][commen.KEY_DOWNLOAD_LIST]

        self.done_list = list()
        self._sub = None
        self._sub_mem = None

        self._process_dict = dict()
        self._process_dict[self.DOWNLOADER] = list()
        self._process_dict[self.SCENE_SPLITTER] = list()
        self._process_dict[self.FACE_SPLITTER] = list()
        self._process_dict[self.FACE_SLICER] = list()
        self._process_dict[self.AUDIO_EXTRACTOR] = list()
        self._process_dict[self.TEXT_CREATOR] = list()

        self._download_targets = list()
        self._scene_targets = list()
        self._face_splitter_targets = list()
        self._face_slicer_targets = list()
        self._audio_targets = list()
        self._text_targets = list()

        self._action_pointer = None

        self._download_done = list()
        self._scenes_done = list()
        self._face_splitter_done = list()
        self._face_slicer_done = list()
        self._audio_done = list()
        self._text_done = list()

        self._config = None

        signal.signal(signal.SIGINT, self.signal_handler)
        self._max_time = config[commen.MAX_TIME] if commen.MAX_TIME in config.keys() else None

        self._end = False
        self._write_end_fiel()

    def _write_end_fiel(self):
        with open("./end.txt", "w") as f:
            f.write("False")

    def _read_end_file(self):
        with open("./end.txt", "r") as f:
            content = f.readline()
            content = content.replace("\n", "")
            if content == "True":
                self._end = True

    def __show_process_dict(self, key):
        n = len(self._process_dict[key]) if key in self._process_dict else 0
        print("\t" + key + ": " + str(n) + " " + str(self.get_run_time_list(key, as_str=True)))

    def show_process_dict(self):
        print()
        print(str(datetime.datetime.now()))
        print("running:")
        self.__show_process_dict(self.DOWNLOADER)
        self.__show_process_dict(self.SCENE_SPLITTER)
        self.__show_process_dict(self.FACE_SPLITTER)
        self.__show_process_dict(self.FACE_SLICER)
        self.__show_process_dict(self.AUDIO_EXTRACTOR)
        self.__show_process_dict(self.TEXT_CREATOR)

    def load_config(self, reload=False):
        if self._config is None or reload:
            with open(self.config_path) as f:
                self._config = yaml.load(f, Loader=yaml.FullLoader)

    def make_paths(self):
        os.makedirs(self._config[self.DOWNLOADER][commen.KEY_OUTPUT_DIR], exist_ok=True)
        os.makedirs(self._config[self.SCENE_SPLITTER][commen.KEY_OUTPUT_DIR], exist_ok=True)
        os.makedirs(self._config[self.FACE_SPLITTER][commen.KEY_OUTPUT_DIR], exist_ok=True)
        os.makedirs(self._config[self.FACE_SLICER][commen.KEY_OUTPUT_DIR], exist_ok=True)
        os.makedirs(self._config[self.AUDIO_EXTRACTOR][commen.KEY_OUTPUT_DIR], exist_ok=True)
        os.makedirs(self._config[self.TEXT_CREATOR][commen.KEY_OUTPUT_DIR], exist_ok=True)

    def kill_overtime_processes(self):

        change = False

        if self._max_time is None:
            return change

        current_time = time.time()

        for key in self._process_dict.keys():
            remove_list = list()
            for i, content in enumerate(self._process_dict[key]):
                p, target, conn, start_time = content
                if int(current_time - start_time) > self._max_time:
                    print("killing " + str(key) + " process at " + str(target) + " (overtime)")
                    change = True
                    remove_list.append(i)
                    try:
                        p.kill()
                    except Exception as e:
                        print("kill failed")
                        print(str(e))
                    if conn is not None:
                        try:
                            conn.close()
                        except:
                            pass
                    self.write_failed_file(self._config[key][commen.KEY_OUTPUT_DIR], str(target))
            for i in reversed(remove_list):
                self._process_dict[key].pop(i)

        return change

    def check_for_finished_process(self, show_update=False):
        change = False
        if self.DOWNLOADER in self._process_dict:
            remove_index = list()

            for i, content in enumerate(self._process_dict[self.DOWNLOADER]):
                p, target, conn, start_time = content
                if p.is_alive():
                    continue

                remove_index.append(i)
                try:
                    temp = conn.recv()

                    if temp == "failed":
                        self.write_failed_file(self._config[self.DOWNLOADER][commen.KEY_OUTPUT_DIR], target)
                        continue

                    if temp.endswith(".mp4"):
                        temp = temp[:-4]
                    print("finished: " + str(self.DOWNLOADER) + " at " + str(temp))
                    self.write_done_file(self._config[self.DOWNLOADER][commen.KEY_OUTPUT_DIR], temp)
                except Exception as e:
                    print(str(e))
                    print("error on download")

            for i in reversed(remove_index):
                self._process_dict[self.DOWNLOADER].pop(i)
                change = True

        if self.SCENE_SPLITTER in self._process_dict:

            remove_index = list()

            for i, content in enumerate(self._process_dict[self.SCENE_SPLITTER]):
                p, target, conn, start_time = content
                if p.is_alive():
                    continue

                print("finished: " + str(self.SCENE_SPLITTER) + " at " + str(target))
                self.write_done_file(self._config[self.SCENE_SPLITTER][commen.KEY_OUTPUT_DIR], target)
                remove_index.append(i)

            for i in reversed(remove_index):
                self._process_dict[self.SCENE_SPLITTER].pop(i)
                change = True

        if self.FACE_SPLITTER in self._process_dict:
            remove_index = list()

            for i, content in enumerate(self._process_dict[self.FACE_SPLITTER]):
                p, target, conn, start_time = content
                if p.is_alive():
                    continue

                print("finished: " + str(self.FACE_SPLITTER) + " at " + str(target))
                self.write_done_file(self._config[self.FACE_SPLITTER][commen.KEY_OUTPUT_DIR], target)
                remove_index.append(i)

            for i in reversed(remove_index):
                self._process_dict[self.FACE_SPLITTER].pop(i)
                change = True

        if self.FACE_SLICER in self._process_dict:
            remove_index = list()

            for i, content in enumerate(self._process_dict[self.FACE_SLICER]):
                p, target, conn, start_time = content
                if p.is_alive():
                    continue

                print("finished: " + str(self.FACE_SLICER) + " at " + str(target))
                self.write_done_file(self._config[self.FACE_SLICER][commen.KEY_OUTPUT_DIR], target)
                remove_index.append(i)

            for i in reversed(remove_index):
                self._process_dict[self.FACE_SLICER].pop(i)
                change = True

        if self.AUDIO_EXTRACTOR in self._process_dict:
            remove_index = list()
            for i, content in enumerate(self._process_dict[self.AUDIO_EXTRACTOR]):
                p, target, conn, start_time = content
                if p.is_alive():
                    continue

                print("finished: " + str(self.AUDIO_EXTRACTOR) + " at " + str(target))
                self.write_done_file(self._config[self.AUDIO_EXTRACTOR][commen.KEY_OUTPUT_DIR], target)
                remove_index.append(i)

            for i in reversed(remove_index):
                self._process_dict[self.AUDIO_EXTRACTOR].pop(i)
                change = True

        if self.TEXT_CREATOR in self._process_dict:
            remove_index = list()
            for i, content in enumerate(self._process_dict[self.TEXT_CREATOR]):
                p, target, conn, start_time = content
                if p.is_alive():
                    continue

                print("finished: " + str(self.TEXT_CREATOR) + " at " + str(target))
                self.write_done_file(self._config[self.TEXT_CREATOR][commen.KEY_OUTPUT_DIR], target)
                remove_index.append(i)

            for i in reversed(remove_index):
                self._process_dict[self.TEXT_CREATOR].pop(i)
                change = True

        overtime_change = self.kill_overtime_processes()

        if change or overtime_change or show_update:
            self.show_process_dict()

    def _update_action_pointer(self):
        self._action_pointer = self.DOWNLOADER

        if self._action_pointer == self.DOWNLOADER:
            if len(self._process_dict[self.DOWNLOADER]) <= 0 and len(self._download_targets) <= 0:
                self._action_pointer = self.SCENE_SPLITTER
        else:
            return

        if self._action_pointer == self.SCENE_SPLITTER:
            if len(self._process_dict[self.SCENE_SPLITTER]) <= 0 and len(self._scene_targets) <= 0:
                self._action_pointer = self.FACE_SPLITTER
        else:
            return

        if self._action_pointer == self.FACE_SPLITTER:
            if len(self._process_dict[self.FACE_SPLITTER]) <= 0 and len(self._face_splitter_targets) <= 0:
                self._action_pointer = self.FACE_SLICER
        else:
            return

        if self._action_pointer == self.FACE_SLICER:
            if len(self._process_dict[self.FACE_SLICER]) <= 0 and len(self._face_slicer_targets) <= 0:
                self._action_pointer = self.AUDIO_EXTRACTOR
        else:
            return

        if self._action_pointer == self.AUDIO_EXTRACTOR:
            if len(self._process_dict[self.AUDIO_EXTRACTOR]) <= 0 and len(self._audio_targets) <= 0:
                self._action_pointer = self.TEXT_CREATOR
        else:
            return

        if self._action_pointer == self.TEXT_CREATOR:
            if len(self._process_dict[self.TEXT_CREATOR]) <= 0 and len(self._text_targets) <= 0:
                self._action_pointer = self.DONE
        else:
            return

    def _get_in_action_list(self, key):
        if key not in self._process_dict:
            return []
        res = []
        for p, target, conn, start_time in self._process_dict[key]:
            res.append(target)
        return res

    def _is_target_in_black_list(self, target, key):
        working = self._get_global_working_targets()
        dones = self.read_done_file(self._config[key][commen.KEY_OUTPUT_DIR])
        fails = self.read_failed_file(self._config[key][commen.KEY_OUTPUT_DIR])

        return target in working or target in dones or target in fails

    def start_downloader(self):
        if self._action_pointer != self.DOWNLOADER:
            return

        if self.DOWNLOADER not in self._process_dict:
            self._process_dict[self.DOWNLOADER] = list()

        if len(self._process_dict[self.DOWNLOADER]) >= self._config[self.DOWNLOADER][self.PROCESS_COUNT]:
            return

        if len(self._download_targets) <= 0:
            return

        while len(self._download_targets) > 0:
            target = self._download_targets.pop(random.randint(0, len(self._download_targets) - 1))

            temp_target = target.split("=")[-1]

            if not self._is_target_in_black_list(temp_target, self.DOWNLOADER):
                break
            elif len(self._download_targets) == 0:
                return

        ok = True
        for p, temp_target, conn, start_time in self._process_dict[self.DOWNLOADER]:
            if target == temp_target:
                ok = False
                break

        if not ok:
            self.start_downloader()
            return

        parent_conn, child_conn = Pipe()
        p = Process(target=download_run, args=(self.config_path, target, child_conn))
        self._process_dict[self.DOWNLOADER].append((p, target,  parent_conn, time.time()))

        print("start " + self.DOWNLOADER + " at: " + str(target))
        p.start()
        self.show_process_dict()
        self.start_downloader()

    def start_scene_splitter(self):
        if self.queued and self._action_pointer != self.SCENE_SPLITTER:
            return

        if self.SCENE_SPLITTER not in self._process_dict:
            self._process_dict[self.SCENE_SPLITTER] = list()

        if len(self._process_dict[self.SCENE_SPLITTER]) >= self._config[self.SCENE_SPLITTER][self.PROCESS_COUNT]:
            return

        if len(self._scene_targets) <= 0:
            return

        while len(self._scene_targets) > 0:
            target = self._scene_targets.pop(random.randint(0, len(self._scene_targets) - 1))

            if not self._is_target_in_black_list(target, self.SCENE_SPLITTER):
                break
            elif len(self._scene_targets) == 0:
                return

        p = Process(target=scene_splitter_run, args=(self.config_path, os.path.join(self._config[self.SCENE_SPLITTER][commen.KEY_INPUT_DIR], str(target) + ".mp4")))
        self._process_dict[self.SCENE_SPLITTER].append((p, target, None, time.time()))

        print("start " + self.SCENE_SPLITTER + " at: " + str(target))
        p.start()
        self.show_process_dict()

    def start_face_splitter(self):
        if self._action_pointer in [self.DOWNLOADER, self.SCENE_SPLITTER]:
            return

        if self.queued and self._action_pointer != self.FACE_SPLITTER:
            return

        if self.FACE_SPLITTER not in self._process_dict:
            self._process_dict[self.FACE_SPLITTER] = list()

        if len(self._process_dict[self.FACE_SPLITTER]) >= self._config[self.FACE_SPLITTER][self.PROCESS_COUNT]:
            return

        if len(self._face_splitter_targets) <= 0:
            return

        while len(self._face_splitter_targets) > 0:
            target_dir = self._face_splitter_targets.pop(random.randint(0, len(self._face_splitter_targets) - 1))

            if not self._is_target_in_black_list(target_dir, self.FACE_SPLITTER):
                break
            elif len(self._face_splitter_targets) == 0:
                return

        p = Process(target=face_splitter_run, args=(self.config_path, os.path.join(self._config[self.FACE_SPLITTER][commen.KEY_INPUT_DIR], target_dir)))
        self._process_dict[self.FACE_SPLITTER].append((p, os.path.split(target_dir)[-1], None, time.time()))

        print("start " + self.FACE_SPLITTER + " at: " + str(target_dir))
        p.start()
        self.show_process_dict()
        self.start_face_splitter()

    def start_face_slicer(self):
        if self._action_pointer in [self.DOWNLOADER, self.SCENE_SPLITTER]:
            return
        if self.queued and self._action_pointer != self.FACE_SLICER:
            return

        if self.FACE_SLICER not in self._process_dict:
            self._process_dict[self.FACE_SLICER] = list()

        if len(self._process_dict[self.FACE_SLICER]) >= self._config[self.FACE_SLICER][self.PROCESS_COUNT]:
            return

        if len(self._face_slicer_targets) <= 0:
            return

        target_dir = self._face_slicer_targets.pop(random.randint(0, len(self._face_slicer_targets) - 1))

        while len(self._face_slicer_targets) > 0:
            target_dir = self._face_slicer_targets.pop(random.randint(0, len(self._face_slicer_targets) - 1))

            if not self._is_target_in_black_list(target_dir, self.FACE_SLICER):
                break
            elif len(self._face_slicer_targets) == 0:
                return

        p = Process(target=slicer_run, args=(self.config_path, os.path.join(self._config[self.FACE_SLICER][commen.KEY_INPUT_DIR], target_dir)))
        self._process_dict[self.FACE_SLICER].append((p, os.path.split(target_dir)[-1], None, time.time()))

        print("start " + self.FACE_SLICER + " at: " + str(target_dir))
        p.start()
        self.show_process_dict()
        self.start_face_slicer()

    def start_audio_extractor(self):
        if self._action_pointer in [self.DOWNLOADER, self.SCENE_SPLITTER]:
            return
        if self.queued and self._action_pointer != self.AUDIO_EXTRACTOR:
            return

        if self.AUDIO_EXTRACTOR not in self._process_dict:
            self._process_dict[self.AUDIO_EXTRACTOR] = list()

        if len(self._process_dict[self.AUDIO_EXTRACTOR]) >= self._config[self.AUDIO_EXTRACTOR][self.PROCESS_COUNT]:
            return

        if len(self._audio_targets) <= 0:
            return

        while len(self._audio_targets) > 0:
            target_dir = self._audio_targets.pop(random.randint(0, len(self._audio_targets) - 1))

            if not self._is_target_in_black_list(target_dir, self.AUDIO_EXTRACTOR):
                break
            elif len(self._audio_targets) == 0:
                return


        p = Process(target=audio_extractor_run, args=(self.config_path, os.path.join(self._config[self.AUDIO_EXTRACTOR][commen.KEY_INPUT_DIR], target_dir)))
        self._process_dict[self.AUDIO_EXTRACTOR].append((p, os.path.split(target_dir)[-1], None, time.time()))

        print("start " + self.AUDIO_EXTRACTOR + " at: " + str(target_dir))
        p.start()
        self.show_process_dict()

        self.start_audio_extractor()

    def start_text_creator(self):
        if self._action_pointer in [self.DOWNLOADER, self.SCENE_SPLITTER]:
            return
        if self.queued and self._action_pointer != self.TEXT_CREATOR:
            return

        if self.TEXT_CREATOR not in self._process_dict:
            self._process_dict[self.TEXT_CREATOR] = list()

        if len(self._process_dict[self.TEXT_CREATOR]) >= self._config[self.TEXT_CREATOR][self.PROCESS_COUNT]:
            return

        if len(self._text_targets) <= 0:
            return

        while len(self._text_targets) > 0:
            target_dir = self._text_targets.pop(random.randint(0, len(self._text_targets) - 1))

            if not self._is_target_in_black_list(target_dir, self.TEXT_CREATOR):
                break
            elif len(self._text_targets) == 0:
                return


        p = Process(target=text_creator_run, args=(self.config_path, os.path.join(self._config[self.TEXT_CREATOR][commen.KEY_INPUT_DIR], target_dir)))
        self._process_dict[self.TEXT_CREATOR].append((p, os.path.split(target_dir)[-1], None, time.time()))

        print("start " + self.TEXT_CREATOR + " at: " + str(target_dir))
        p.start()
        self.show_process_dict()

        self.start_text_creator()

    def get_run_time_list(self, key, as_str=False):
        run_time = list()
        current_time = time.time()
        for _, _, _, start_time in self._process_dict[key]:
            run_time.append(int(current_time - start_time))

        if as_str:

            res = ""
            for r in run_time:
                res += " " + str(r)
            return res

        return run_time

    def get_combined_process_count(self):
        return len(self._process_dict[self.DOWNLOADER]) \
               + len(self._process_dict[self.SCENE_SPLITTER]) \
               + len(self._process_dict[self.FACE_SPLITTER]) \
               + len(self._process_dict[self.FACE_SLICER]) \
               + len(self._process_dict[self.AUDIO_EXTRACTOR]) \
               + len(self._process_dict[self.TEXT_CREATOR])

    def print_process_count(self):
        print()
        print(str(datetime.datetime.now()))
        print(str(len(self._process_dict[self.DOWNLOADER])) + " " + self.DOWNLOADER + " " + self.get_run_time_list(self.DOWNLOADER, as_str=True))
        print(str(len(self._process_dict[self.SCENE_SPLITTER])) + " " + self.SCENE_SPLITTER + " " + self.get_run_time_list(self.SCENE_SPLITTER, as_str=True))
        print(str(len(self._process_dict[self.FACE_SPLITTER])) + " " + self.FACE_SPLITTER + " " + self.get_run_time_list(self.FACE_SPLITTER, as_str=True))
        print(str(len(self._process_dict[self.FACE_SLICER])) + " " + self.FACE_SLICER + " " + self.get_run_time_list(self.FACE_SLICER, as_str=True))
        print(str(len(self._process_dict[self.AUDIO_EXTRACTOR])) + " " + self.AUDIO_EXTRACTOR + " " + self.get_run_time_list(self.AUDIO_EXTRACTOR, as_str=True))
        print(str(len(self._process_dict[self.TEXT_CREATOR])) + " " + self.TEXT_CREATOR + " " + self.get_run_time_list(self.TEXT_CREATOR, as_str=True))

    def read_done_file(self, target_dir):
        if not os.path.exists(os.path.join(target_dir, "done.txt")):
            # print("found no 'done.txt' in " + str(target_dir))
            return []

        content = list()

        with open(os.path.join(target_dir, "done.txt"), "r") as f:

            temp = f.readline()

            while temp is not None and temp != "":
                if temp.endswith("\n"):
                    temp = temp[:-1]
                content.append(temp)
                temp = f.readline()

        return content

    def read_failed_file(self, target_dir):
        if not os.path.exists(os.path.join(target_dir, "failed.txt")):
            return []

        content = list()

        with open(os.path.join(target_dir, "failed.txt"), "r") as f:

            temp = f.readline()

            while temp is not None and temp != "":
                if temp.endswith("\n"):
                    temp = temp[:-1]
                if "=" in temp:
                    temp = temp.split("=")[-1]
                content.append(temp)
                temp = f.readline()

        return content

    def write_failed_file(self, target_dir, targets):
        if not isinstance(targets, list):
            targets = [targets]
        with open(os.path.join(target_dir, "failed.txt"), "a") as f:
            for t in targets:
                f.write(str(t) + "\n")

    def write_done_file(self, target_dir, targets):
        if not isinstance(targets, list):
            targets = [targets]
        with open(os.path.join(target_dir, "done.txt"), "a") as f:
            for t in targets:
                f.write(str(t) + "\n")

    def clean_up_ruins(self, target_dir):
        done_content = self.read_done_file(target_dir)

        for d in os.listdir(os.path.join(target_dir)):
            if os.path.isdir(os.path.join(target_dir, d)) and d not in done_content:
                os.rmdir(os.path.join(target_dir, d))

    def read_done_lists(self):
        self._download_done = self.read_done_file(self._config[self.DOWNLOADER][commen.KEY_OUTPUT_DIR])
        self._scenes_done = self.read_done_file(self._config[self.SCENE_SPLITTER][commen.KEY_OUTPUT_DIR])
        self._face_splitter_done = self.read_done_file(self._config[self.FACE_SPLITTER][commen.KEY_OUTPUT_DIR])
        self._face_slicer_done = self.read_done_file(self._config[self.FACE_SLICER][commen.KEY_OUTPUT_DIR])
        self._audio_done = self.read_done_file(self._config[self.AUDIO_EXTRACTOR][commen.KEY_OUTPUT_DIR])
        self._text_done = self.read_done_file(self._config[self.TEXT_CREATOR][commen.KEY_OUTPUT_DIR])

    def read_failed_lists(self):
        self._download_failed = self.read_failed_file(self._config[self.DOWNLOADER][commen.KEY_OUTPUT_DIR])
        self._scenes_failed = self.read_failed_file(self._config[self.SCENE_SPLITTER][commen.KEY_OUTPUT_DIR])
        self._face_splitter_failed = self.read_failed_file(self._config[self.FACE_SPLITTER][commen.KEY_OUTPUT_DIR])
        self._face_slicer_failed = self.read_failed_file(self._config[self.FACE_SLICER][commen.KEY_OUTPUT_DIR])
        self._audio_failed = self.read_failed_file(self._config[self.AUDIO_EXTRACTOR][commen.KEY_OUTPUT_DIR])
        self._text_failed = self.read_failed_file(self._config[self.TEXT_CREATOR][commen.KEY_OUTPUT_DIR])

    def update_targets(self):
        self.read_done_lists()
        self.read_failed_lists()

        # download stuff
        # print(self._download_done)
        global_working_targets = self._get_global_working_targets()

        with open(self.target_file, "r") as file:
            file.seek(0)
            # print("reading: " + str(self.target_file))
            while True:

                temp = file.readline()
                temp = temp.replace("\n", "")
                # print(temp)

                if temp == "":
                    break

                if temp.endswith(".txt"):
                    continue

                if temp.split("=")[-1] in self._download_done:
                    continue

                if temp.split("=")[-1] in self._download_failed:
                    continue

                if temp.split("=")[-1] in global_working_targets:
                    continue

                # print("added: " + str(temp))
                self._download_targets.append(temp)
            file.close()

        self._scene_targets = self._update_target_list(self._scene_targets, self.SCENE_SPLITTER, [self._scenes_done, self._scenes_failed, global_working_targets], file_target=True)
        self._face_splitter_targets = self._update_target_list(self._face_splitter_targets, self.FACE_SPLITTER, [self._face_splitter_done, global_working_targets, self._face_splitter_failed])
        self._face_slicer_targets = self._update_target_list(self._face_slicer_targets, self.FACE_SLICER, [self._face_slicer_done, self._face_slicer_failed, global_working_targets])
        self._audio_targets = self._update_target_list(self._audio_targets, self.AUDIO_EXTRACTOR, [self._audio_done, self._audio_failed, global_working_targets])
        self._text_targets = self._update_target_list(self._text_targets, self.TEXT_CREATOR, [self._text_done, self._text_failed, global_working_targets])

     # print("download_targets:")
        # print(self._download_targets)
        # print("scene_targets:")
        # print(self._scene_targets)
        # print("face_splitter_targets:")
        # print(self._face_splitter_targets)
        # print("face_face_slicer_targets:")
        # print(self._face_slicer_targets)
        # print("audio_targets:")
        # print(self._audio_targets)
        # print("text_targets:")
        # print(self._text_targets)

    def _get_global_working_targets(self):
        res_list = list()
        for key in self._process_dict:
            for content in self._process_dict[key]:
                target = str(content[1])
                if "=" in target:
                    target = target.split("=")[-1]


                res_list.append(target)
        return res_list

    def _update_target_list(self, target_list, key, back_lists, file_target=False):
        input_dir = self._config[key][commen.KEY_INPUT_DIR]

        for f in os.listdir(input_dir):
            if (file_target and os.path.isfile(os.path.join(input_dir, f))) or (not file_target and os.path.isdir(os.path.join(input_dir, f))):
                if f.endswith(".txt"):
                    continue
                if ".part" in f:
                    continue

                if ".ytdl" in f:
                    continue

                if f.split(".")[-1] == "mp4":
                    if f.split(".")[-1] == "mp4":

                        f = f.split(".")[0]
                    else:
                        continue
                ok = True
                for bl in back_lists:
                    if f in bl:
                        ok = False
                        break
                if not ok:
                    continue

                if f in target_list:
                    continue

                target_list.append(f)

        for bl in back_lists:
            for d in bl:
                d = str(d)
                if d in target_list:
                    target_list.remove(d)

        return target_list

    def run_with_download(self, verbose=False):
        self.load_config()
        self.make_paths()

        self._action_pointer = self.DOWNLOADER
        active = True
        i = 0

        while active:
            if i % 10 == 0:
                self.load_config()
                i = 0
            i += 1
            self._read_end_file()

            if self._end:
                print("shutting down soon")
                self.check_for_finished_process()
                p_running = self.get_combined_process_count()
                print("processes runing:", str(p_running))
                if p_running == 0:
                    self._action_pointer = self.DONE
                    print("done")
            else:
                self.update_targets()

                self.start_downloader()
                self.start_scene_splitter()
                self.start_face_splitter()
                self.start_face_slicer()
                self.start_audio_extractor()
                self.start_text_creator()

                self.check_for_finished_process(i % 5 == 0)
                self._update_action_pointer()

                if verbose:
                    self.print_process_count()

            if self._action_pointer == self.DONE:
                active = False
            else:
                time.sleep(10)

    def run_without_download(self, verbose=False):
        self._action_pointer = self.SCENE_SPLITTER
        active = True
        i = 0
        while active:

            if 0 % 10 == 0:
                self.load_config()
                i = 0
            i += 1

            self.start_scene_splitter()
            self.start_face_splitter()
            self.start_face_slicer()
            self.start_audio_extractor()
            self.start_text_creator()

            self.check_for_finished_process()
            self._update_action_pointer()

            if verbose:
                self.print_process_count()

            if self._action_pointer == self.DONE:
                active = False
            else:
                time.sleep(10)

    def _remove_current_working_dirs(self, key, target_ext=None):
        if key not in self._process_dict:
            return
        for p, target, con, start_tie in self._process_dict[key]:

            if target_ext is not None:
                target = target + target_ext
            try:
                p.stop()
            except:
                pass

            try:
                os.rmdir(os.path.join(self._config[key][commen.KEY_OUTPUT_DIR], target + "_temp"))
            except:
                pass

    def signal_handler(self, signal, frame):
        self._remove_current_working_dirs(self.DOWNLOADER, ".mp4")
        self._remove_current_working_dirs(self.SCENE_SPLITTER)
        self._remove_current_working_dirs(self.FACE_SPLITTER)
        self._remove_current_working_dirs(self.FACE_SLICER)
        self._remove_current_working_dirs(self.AUDIO_EXTRACTOR)
        self._remove_current_working_dirs(self.TEXT_CREATOR)
        sys.exit(0)


def write_done_from_dirs():
    target_dir = """/media/ubuntu/data/lip_reading_data/scenes"""
    targets = os.listdir(target_dir)

    ToolManager(False).write_done_file(target_dir, targets)


def main():
    ToolManager(False).run_with_download(False)


def some_testing():
    dir = """/media/ubuntu/data/lip_reading_data/testing"""
    target = "blub"

    ToolManager(False).write_done_file(dir, target)
    d = ToolManager(False).read_done_file(dir)
    print(d)


if __name__ == '__main__':
    main()
