import youtube_dl
import sys


import commen

def run(config_path, target, conn):


    try:
        my_key = "downloader"
        data = commen.load_config(config_path, my_key)
        # print(data)
        format = data[commen.KEY_VIDEO_FORMAT]

        # ydl = youtube_dl.YoutubeDL({'outtmpl': data["output_dir"] + "/%(id)s.%(ext)s",
        #                             "format": "worstvideo[ext=mp4][vcodec!*=av01]+bestaudio[ext=m4a]"})


        ydl = youtube_dl.YoutubeDL({'outtmpl': data["output_dir"] + "/%(id)s.%(ext)s",
                                    "format": "bestvideo[height<=" + str(format) + "][ext=mp4][vcodec!*=av01]+bestaudio[ext=m4a]"})

        with ydl:
            result = ydl.extract_info(target, download=True)
            temp = result["id"] + "." + result["ext"]

        if conn is not None:
            conn.send(temp)
            conn.close()
    except:
        conn.send("failed")
        conn.close()

def some_test():
    url = """https://www.youtube.com/watch?v=FwQHaaG6x-c"""
    config = "./config.yaml"
    run(config, url, None)

def main():
    if len(sys.argv) != 3:
        print("need exactly 2 args (config-path) and (taget_file)")

    config_path = sys.argv[1]
    target_file = sys.argv[2]

    run(config_path, target_file, None)


if __name__ == '__main__':
    main()