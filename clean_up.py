import os
import shutil


def is_in_all_lists(key, lists:tuple):
    for l in lists:
        if key not in l:
            return False
    return True


def eliminator(*args):
    target = args[0]

    res = list()
    for t in target:
        if is_in_all_lists(t, args[1:]):
            res.append(t)
        else:
            print(t)

    return res


def exist_in_all(title:list, endings:list):
    res = list()
    for t in title:
        ok = True
        for e in endings:
            if not os.path.exists(t + e):
                ok = False
                break
        if ok:
            res.append(t)
    return res


def get_mp4_list(parent_dir):
    res = list()
    for sub in os.listdir(parent_dir):
        temp_path = os.path.join(parent_dir, sub)
        if os.path.isdir(temp_path):
            res += get_mp4_list(temp_path)
        elif os.path.isfile(temp_path) and os.path.splitext(temp_path)[-1] == ".mp4":
            res.append(os.path.splitext(temp_path)[0])
    return res


def main(add_dir_name=False, max_copys=None):
    source = "/media/ubuntu/data/combined/temp"
    dest = "/media/ubuntu/data/combined/pre_train"

    # ext_list = [".wav", ".txt", ".mp4", ".yaml"]
    # ext_list = [".wav", ".txt", ".mp4"]
    ext_list = [".txt", ".mp4"]

    video = get_mp4_list(source)

    res = exist_in_all(video, ext_list)

    for x, r in enumerate(res):

        if max_copys is not None and x >= max_copys:
            break

        file_name = os.path.split(r)[-1]
        if add_dir_name:
            name_ext = os.path.split(os.path.split(r)[0])[-1] + "_"
        else:
            name_ext = ""

        for e in ext_list:
            temp_dest = os.path.join(dest, name_ext + str(file_name) + e)
            temp_source = os.path.join(str(r) + e)
            print("from", temp_source, "to", temp_dest)
            # print("to", temp_dest)
            shutil.copy(temp_source, temp_dest)


def get_word(text_file):
    with open(text_file, "r") as file:
        text = file.readline()
        text = text.replace("\n", "")
        text = text.replace("Text:", "")
        text = text.replace(" ", "")

    return text

def word_sorter(main_dir, out_dir, rm_old=False):
    video_path_list = get_mp4_list(main_dir)

    for video_path in video_path_list:
        text_path = video_path + ".txt"
        video_path = video_path + ".mp4"
        word = get_word(text_path)
        bucket_path = os.path.join(out_dir, word)
        new_text_path = os.path.join(bucket_path, os.path.split(text_path)[-1])
        new_video_path = os.path.join(bucket_path, os.path.split(video_path)[-1])

        os.makedirs(bucket_path, exist_ok=True)
        if rm_old:
            print("move ", video_path, " to ", new_video_path)
            shutil.move(text_path, new_text_path)
            shutil.move(video_path, new_video_path)
        else:
            print("copy ", video_path, " to ", new_video_path)
            shutil.copy(text_path, new_text_path)
            shutil.copy(video_path, new_video_path)


def make_buckets():
    source_dir = """/media/ubuntu/data/combined/temp"""
    target_dir = """/media/ubuntu/data/combined/pre_train"""
    rm_old = True
    word_sorter(source_dir, target_dir, rm_old)

if __name__ == '__main__':
    main(False)
