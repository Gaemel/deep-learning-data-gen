import cv2
import numpy as np
import face_recognition
from copy import deepcopy
from os import path


def cut_scenes(cap, new_scene_threshold=40, min_scene_length=100):
    success, past_frame = cap.read()

    scenes = list()
    current_scene = [past_frame]

    while success:
        success, frame = cap.read()
        if not success:
            current_scene.append(frame)
            break

        change = cv2.absdiff(frame, past_frame)

        if change.mean() >= new_scene_threshold:
            if len(current_scene) >= min_scene_length:
                scenes.append(current_scene)
            current_scene = []

        current_scene.append(frame)
        past_frame = deepcopy(frame)

    return scenes


def select_faces(frame_list, face_threshold=0.25, image_scale=2, watch_interval=5):
    new_list = list()

    for i, frames in enumerate(frame_list):
        hit_count = 0

        for c, frame in enumerate(frames):
            if c % watch_interval != 0:
                continue

            rgb_frame = frame[:, :, ::-1]
            small_frame = cv2.resize(rgb_frame, (0, 0), fx=1 / image_scale, fy=1 / image_scale)
            face_locations = face_recognition.face_locations(small_frame)

            if len(face_locations) > 0:
                hit_count += 1

        # print(str(hit_count) + "/" + str(int(len(frames)/watch_interval)))

        if (hit_count / (len(frames) / watch_interval)) >= face_threshold:
            new_list.append(frames)
            # print("accepted")

    return new_list


def select_by_length(frame_list, fps, min_len=5, max_len=None):
    return [x for x in frame_list if (len(x) >= min_len*fps and (max_len is None or len(x) <= max_len*fps))]


def split_singular_faces_and_groups(scenes, watch_interval=5, image_scale=2):
    singular_faces = list()
    group_faces = list()

    for scene in scenes:

        singular_face = True

        for c, frame in enumerate(scene):

            if c % watch_interval != 0:
                continue

            rgb_frame = frame[:, :, ::-1]
            small_frame = cv2.resize(rgb_frame, (0, 0), fx=1 / image_scale, fy=1 / image_scale)
            face_locations = face_recognition.face_locations(small_frame)

            if len(face_locations) > 1:
                singular_face = False
                group_faces.append(scene)
                break

        if singular_face:
            singular_faces.append(scene)

    return singular_faces, group_faces


def cut_singular_faces(scenes, watch_interval=5, image_scale=2):
    new_list = list()

    for scene in scenes:
        pos_list, face_first_frame = locate_singular_face(scene, watch_interval, image_scale)
        if pos_list is None:
            continue

        new_list.append(cut_section(scene, pos_list, face_first_frame))

    return new_list


def locate_singular_face(frames, watch_interval=5, image_scale=2):
    face_first_frame = None

    pos_list = list()

    for c, frame in enumerate(frames):
        if c % watch_interval != 0:
            continue

        rgb_frame = frame[:, :, ::-1]
        small_frame = cv2.resize(rgb_frame, (0, 0), fx=1 / image_scale, fy=1 / image_scale)
        face_locations = face_recognition.face_locations(small_frame)

        if len(face_locations) == 1:

            if face_first_frame is None:
                face_first_frame = c

            pos_list.append(np.array(face_locations[0]) * image_scale)

        elif len(face_locations) > 1:
            return None, None

    return pos_list, face_first_frame


def identify_all_faces(frames, watch_interval=5, image_scale=2):
    known_face_encodings = list()
    pos_list = list()
    face_first_frame = list()

    for c, frame in enumerate(frames):
        if c % watch_interval != 0:
            continue

        rgb_frame = frame[:, :, ::-1]
        small_frame = cv2.resize(rgb_frame, (0, 0), fx=1 / image_scale, fy=1 / image_scale)
        face_locations = face_recognition.face_locations(small_frame)
        face_encodings = face_recognition.face_encodings(small_frame, face_locations)

        for i, face_encoding in enumerate(face_encodings):
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

            if matches.count(True) == 0:
                top, right, bottom, left = face_locations[i]
                rgb_new_face_image = small_frame[top:bottom, left:right]

                temp = face_recognition.face_encodings(rgb_new_face_image)

                for new_face_encoding in temp:
                    known_face_encodings.append(new_face_encoding)
                    face_first_frame.append(c)
                    pos_list.append([np.array(face_locations[i]) * image_scale])
                matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            if len(face_distances) <= 0:
                continue

            best_match_index = np.argmin(face_distances)

            if matches.count(True) != 0 and matches[best_match_index]:
                pos_list[best_match_index].append(np.array(face_locations[i]) * image_scale)

                # pos_list[best_match_index][top_index] = min(pos_list[best_match_index][top_index], face_locations[i][top_index]*image_scale)
                # pos_list[best_match_index][right_index] = max(pos_list[best_match_index][right_index], face_locations[i][right_index]*image_scale)
                # pos_list[best_match_index][bottom_index] = max(pos_list[best_match_index][bottom_index], face_locations[i][bottom_index]*image_scale)
                # pos_list[best_match_index][left_index] = min(pos_list[best_match_index][left_index], face_locations[i][left_index]*image_scale)

    return pos_list, face_first_frame


def cut_section(frames, locations, start_frame, up_scale=1.5, buffer_size=5, over_size_scale_steps=0.1):

    top_index = 0
    right_index = 1
    bottom_index = 2
    left_index = 3

    locations = np.array(locations)

    max_height = (locations[:, bottom_index] - locations[:, top_index]).max()
    max_width = (locations[:, right_index] - locations[:, left_index]).max()

    max_height = int(min(max_height*up_scale, frames[0].shape[0]))
    max_width = int(min(max_width*up_scale, frames[0].shape[1]))

    max_height = max(max_height, max_width)
    max_width = max(max_height, max_width)

    center_height = int((locations[:, bottom_index].mean() + locations[:, top_index].mean()) / 2)
    center_width = int((locations[:, left_index].mean() + locations[:, right_index].mean()) / 2)

    top = int(center_height - max_height / 2)
    right = int(center_width + max_width / 2)
    bottom = int(center_height + max_height / 2)
    left = int(center_width - max_width / 2)

    if top < 0 or left < 0 or bottom >= frames[0].shape[0] or right >= frames[0].shape[1]:
        return cut_section(frames, locations, start_frame, up_scale - over_size_scale_steps, buffer_size,
                           over_size_scale_steps)

    new_frames = list()

    for frame in frames[start_frame:]:
        section = frame[top:bottom, left:right]
        new_frames.append(section)

        # print(section.shape)
        # cv2.imshow("test", section)
        # cv2.waitKey(10)

    return new_frames


def cut_singular_moving_faces_from_group(scenes, watch_interval=5, image_scale=2, movement_threshold_image=1, movement_threshold=0.8, **kwargs):
    new_list = list()

    for frames in scenes:

        pos_list, spawn_index = identify_all_faces(frames, watch_interval, image_scale)

        singular_moving_face = None

        for i in range(len(pos_list)):

            section_list = cut_section(frames, pos_list[i], spawn_index[i], **kwargs)
            past_frame = None
            movement_count = 0

            for c, section in enumerate(section_list):
                if c % watch_interval != 0:
                    continue

                gray = cv2.cvtColor(section, cv2.COLOR_BGR2GRAY)
                if past_frame is not None:
                    change = cv2.absdiff(gray, past_frame)
                    # cv2.imshow("change", change)
                    # cv2.waitKey(20)
                    if change.mean() >= movement_threshold_image:
                        movement_count += 1

                past_frame = gray

            # print(movement_count * watch_interval / len(section_list))
            if movement_count * watch_interval / len(section_list) >= movement_threshold:

                if singular_moving_face is not None:
                    singular_moving_face = None
                    break
                else:
                    singular_moving_face = section_list

        if singular_moving_face is not None:
            new_list.append(singular_moving_face)

    return new_list


def save_vid(target, destination, fps, ext=".mp4"):
    if not isinstance(target[0], list):
        target = [target]

    if ext == ".avg":
        fourcc = cv2.VideoWriter_fourcc(*"XVID")
    else:
        ext = ".mp4"
        fourcc = cv2.VideoWriter_fourcc(*"mp4v")

    for c, t in enumerate(target):
        height, wigth = t[0].shape[:-1]
        shape = (wigth, height)
        temp_path = path.join(destination, "test_" + str(c) + ext)
        out = cv2.VideoWriter(temp_path, fourcc, fps, shape)
        print("saveing: " + str(temp_path))
        for frame in t:
            out.write(frame)

        out.release()


def foo_0():
    cap = cv2.VideoCapture('tagesschau_001.mp4')
    fps = cap.get(cv2.CAP_PROP_FPS)

    scenes = cut_scenes(cap)
    scenes = select_by_length(scenes, fps, 5)
    save_vid(scenes, "/home/ubuntu/PycharmProjects/Video_Data_Miner/scenes_0", fps)
    scenes = select_faces(scenes)
    save_vid(scenes, "/home/ubuntu/PycharmProjects/Video_Data_Miner/scenes_1", fps)
    singular, group = split_singular_faces_and_groups(scenes)

    save_vid(singular, "/home/ubuntu/PycharmProjects/Video_Data_Miner/scenes_2", fps)
    save_vid(group, "/home/ubuntu/PycharmProjects/Video_Data_Miner/scenes_3", fps)

    scenes = cut_singular_moving_faces_from_group(group)
    save_vid(scenes, "/home/ubuntu/PycharmProjects/Video_Data_Miner/scenes_4", fps)

    scenes = cut_singular_faces(singular)
    save_vid(scenes, "/home/ubuntu/PycharmProjects/Video_Data_Miner/scenes_5", fps)

    cap.release()
    cv2.destroyAllWindows()


def foo_1():
    cap = cv2.VideoCapture('/home/ubuntu/PycharmProjects/Video_Data_Miner/scenes_2/test_8.mp4')
    fps = cap.get(cv2.CAP_PROP_FPS)
    scenes = [[]]

    while True:
        s, f = cap.read()
        if not s:
            break
        scenes[0].append(f)
    cap.release()

    scenes = cut_singular_faces(scenes)
    save_vid(scenes, "/home/ubuntu/PycharmProjects/Video_Data_Miner/scenes_5", fps)

if __name__ == '__main__':
    foo_0()