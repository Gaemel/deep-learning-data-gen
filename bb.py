import cv2
import commen
from copy import copy
import os
import numpy as np
import yaml
from random import shuffle

class BB():

    PT_0 = "pt_0"
    PT_1 = "pt_1"

    def __init__(self, out_dir, target_size=800, watch_interval=5, file_list=None):
        self._out_dir = out_dir
        self._target_scene = None
        self._target_img = None
        self._index = None
        self._points = list()
        self._shown_img = None
        self._target_size = target_size
        self._event_set = False
        self._ration = None
        self._end = False
        self._watch_interval = watch_interval
        self._title_index = 0
        self._target_title = None
        self._annotations = dict()
        self._global_index = 0
        self._source_list = list()
        self._load_source_list()
        self._file_list = file_list
        if self._file_list is not None and isinstance(self._file_list, list):
            shuffle(self._file_list)

    def _load_source_list(self):
        self._source_list = list()
        temp_path = os.path.join(self._out_dir, "annotations.yaml")
        if os.path.exists(temp_path):
            with open(temp_path) as f:
                data = yaml.load(f, Loader=yaml.FullLoader)

                for key in data:
                    key = str(key)
                    temp = key.split("_")
                    temp_key = temp[0]
                    for t in temp[1:-1]:
                        temp_key += "_" + t
                    if temp_key not in self._source_list:
                        self._source_list.append(temp_key)

    def _next_img(self, watch_count=1):
        f = None
        for x in range(watch_count):
            s, f = self._target_scene.read()
            if not s:
                return None
        return f

    def run(self):
        while not self._end:
            self.new_scene()

            i = 0
            while True:
                if self._end:
                    break

                self._target_img = self._next_img(self._watch_interval)
                if self._target_img is None:
                    break

                self._index = i
                print("new img: " + str(self._global_index))
                self._global_index += 1
                self.show()

                i += self._watch_interval

            # for i, img in enumerate(self._target_scene):
            #     if self._end:
            #         break
            #     if i % self._watch_interval != 0:
            #         continue
            #     self._target_img = img
            #     self._index = i
            #     print("new img: " + str(self._global_index))
            #     self._global_index += 1
            #     self.show()

        print("...")

    def new_scene(self):
        if self._file_list is not None:
            self.load_scene(self._file_list.pop(0))
            # this crashes is the list empty... but meh
        else:
            self.write_annotations()
            ok = False
            while not ok:
                target_path = input("enter target path")
                if target_path == "q":
                    self.quit()
                    break
                else:
                    print(target_path)
                    if not os.path.exists(target_path):
                        print("does not exist: " + str(target_path))
                        continue

                    # if from_meta:
                    #     target_path, ext = os.path.splitext(target_path)
                    #     ext = ".yaml"
                    #     target_path = target_path + ext

                    if not os.path.exists(target_path):
                        print("path does not exist")
                        continue

                    if self._is_target_in_source(target_path):
                        print("source was already read")
                        continue

                    try:
                        self.load_scene(target_path=target_path)
                        ok = True
                    except:
                        print("fained to load scene")

    def _is_target_in_source(self, target_path):
        return os.path.splitext(os.path.split(target_path)[-1])[0] in self._source_list

    def click_event(self, event, x, y, flags, params):
        if event == cv2.EVENT_MBUTTONDOWN or event == cv2.EVENT_LBUTTONDOWN:
            x = int(x / self._ration)
            y = int(y / self._ration)

        if event == cv2.EVENT_MBUTTONDOWN:
            self._points.append(np.array((x, y)))
            while len(self._points) > 2:
                self._points.pop(0)

            self.update_show_img()

        elif event == cv2.EVENT_LBUTTONDOWN:
            if len(self._points) < 2:
                new_pos = np.array((x, y))
                self._points.append(new_pos)
            else:
                index_x = 0 if abs(self._points[0][0] - x) < abs(self._points[1][0] - x) else 1
                index_y = 0 if abs(self._points[0][1] - y) < abs(self._points[1][1] - y) else 1

                self._points[index_x][0] = x
                self._points[index_y][1] = y

                # index = 0 if sum(abs(self._points[0] - new_pos)) < sum(abs(self._points[1] - new_pos)) else 1
                # self._points[index] = new_pos

            self.update_show_img()

    def draw(self):
        temp = self.get_pt()

        if temp is None:
            return

        x_min, y_min, x_max, y_max = temp

        x_min = int(x_min * self._ration)
        y_min = int(y_min * self._ration)
        x_max = int(x_max * self._ration)
        y_max = int(y_max * self._ration)

        self._shown_img, self._ration = BB.scale_to(copy(self._target_img), self._target_size)
        self._shown_img = cv2.rectangle(self._shown_img, (x_min, y_min), (x_max, y_max), (0, 255, 0), 1)
        # self.update_show_img()

    def get_pt(self, as_dict=False):
        if len(self._points) != 2:
            return None

        x_min = min(self._points[0][0], self._points[1][0])
        y_min = min(self._points[0][1], self._points[1][1])
        x_max = max(self._points[0][0], self._points[1][0])
        y_max = max(self._points[0][1], self._points[1][1])

        if as_dict:
            return {self.PT_0: {"x": int(x_min), "y": int(y_min)}, self.PT_1: {"x": int(x_max), "y": int(y_max)}}
        else:
            return x_min, y_min, x_max, y_max

    def save(self):
        print("saving...")

        temp = self.get_pt(True)
        if temp is None:
            print("no nounding box")
            return

        os.makedirs(self._out_dir, exist_ok=True)
        temp_title = self._target_title + "_" + str(self._title_index) + ".png"

        self._annotations[temp_title] = temp

        temp_title = os.path.join(self._out_dir, temp_title)

        print("save at: " + str(temp_title))
        cv2.imwrite(temp_title,  self._target_img)

        self._title_index += self._watch_interval
        self.next()

    def next(self):
        self._event_set = False

    def write_annotations(self):
        if len(self._annotations.keys()) == 0:
            return

        temp_path = os.path.join(self._out_dir, "annotations.yaml")
        if os.path.exists(temp_path):
            with open(temp_path) as f:
                data = yaml.load(f, Loader=yaml.FullLoader)

            data.update(self._annotations)
        else:
            data = self._annotations

        with open(temp_path, "w") as file:
            yaml.dump(data, file)

    def quit(self):
        self.write_annotations()
        self._end = True

    def update_show_img(self):
        skip = True
        self.draw()
        cv2.imshow("img", self._shown_img)
        if not self._event_set:
            self._event_set = True
            cv2.setMouseCallback('img', self.click_event)
            while skip:
                temp = cv2.waitKey()

                if chr(temp) == "s":
                    skip = False
                    self.save()
                elif chr(temp) == "n":
                    skip = False
                    self.next()
                elif chr(temp) == "q":
                    skip = False
                    self.quit()

    def show(self):
        self._shown_img, self._ration = BB.scale_to(self._target_img, self._target_size)
        self.update_show_img()

    def load_next_img(self):
        if self._target_scene is None:
            self._target_img = None
            self._index = None
        else:
            if self._index is None:
                self._index = 0
            else:
                self._index += 1

            if len(self._target_scene) <= self._index:
                self._index = None
                self._target_scene = None
                self._target_img = None
            else:
                self._target_img = self._target_scene[self._index]

        return self._target_img

    def load_scene(self, target_path):
        print("start loading scene")
        self._title_index = 0
        self._target_title = os.path.splitext(os.path.split(target_path)[-1])[0]

        self._target_scene = cv2.VideoCapture(target_path)

        self._source_list.append(self._target_title)

    @staticmethod
    def scale_to(img, target_size, absolute=True):
        if target_size is None:
            absolute = False

        if absolute:
            original_size = img.shape[0]
            ratio = target_size / original_size
            new_img = cv2.resize(img, (target_size, target_size))
        else:
            new_img = img
            ratio = 1

        return new_img, ratio


def click_event(event, x, y, flags, params):

    if event == cv2.EVENT_LBUTTONDOWN:
        print(x, '', y)

def main():
    bb = BB("/media/ubuntu/data/lip_reading_data/class_0", watch_interval=20)
    bb.run()


if __name__ == '__main__':
    main()
