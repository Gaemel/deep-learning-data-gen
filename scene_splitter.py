import shutil

import cv2
import yaml
import sys
import os
import commen


def create_scenes_meta(start, end, fps):
    return {commen.KEY_START: start,
            commen.KEY_END: end,
            commen.KEY_VIDEO_FPS: fps}

def cut_to_max_size(scene, max_size):
    if len(scene) <= max_size:
        return [scene]
    else:
        temp_0 = cut_to_max_size(scene[int(len(scene) / 2):], max_size)
        temp_1 = cut_to_max_size(scene[:int(len(scene) / 2)], max_size)
        return temp_1 + temp_0

def cut_scenes(cap, og_video, fps=25, ext=".mp4", *args, **kwargs):
    success, past_frame = cap.read()

    output_dir = os.path.join(kwargs["output_dir"] if "output_dir" in kwargs else "./", os.path.splitext(os.path.split(og_video)[-1])[0])
    new_scene_threshold = kwargs["new_scene_threshold"] if "new_scene_threshold" in kwargs else 40

    min_scene_length = kwargs["min_scene_length"] if "min_scene_length" in kwargs else 4
    min_scene_length = int(min_scene_length * fps)

    max_scene_length = kwargs["max_sence_length"] if "max_sence_length" in kwargs else 20
    max_scene_length = int(max_scene_length * fps)

    max_oversize = kwargs["max_oversize"] if "max_oversize" in kwargs else 20
    max_oversize = int(max_oversize * fps)

    current_scene = [past_frame]
    scene_count = 0
    i = 0
    start = 0
    while success:
        success, frame = cap.read()
        if not success:
            if len(current_scene) >= min_scene_length:

                temp_start = start

                for s in cut_to_max_size(current_scene, max_scene_length):
                    save_scene(s, scene_count, temp_start, output_dir, og_video, fps, ext)
                    scene_count += 1
                    temp_start += len(s)
            return

        change = cv2.absdiff(frame, past_frame)
        if change.mean() >= new_scene_threshold or len(current_scene) >= max_oversize:
            if len(current_scene) >= min_scene_length:

                temp_start = start
                for s in cut_to_max_size(current_scene, max_scene_length):
                    save_scene(s, scene_count, temp_start, output_dir, og_video, fps, ext)
                    scene_count += 1
                    temp_start += len(s)

            current_scene = list()
            start = i

        current_scene.append(frame)
        past_frame = frame
        i += 1


def save_scene(scene, scene_number, start, output_dir, og_video, fps, ext=".mp4"):
    meta = create_scenes_meta(start, start+len(scene), fps)
    # print(meta)
    meta[commen.KEY_ORIGINAL_VIDEO] = og_video

    title = os.path.splitext(os.path.split(og_video)[-1])[0]

    kwargs = {commen.KEY_VIDEO_FPS: fps, commen.KEY_VIDEO_ENDING: ext}
    commen.save(output_dir, title + "_S" + str(scene_number), meta, scene, **kwargs)


def save_vid(target, output_dir, parent_name, fps, og_video, ext=".mp4"):
    meta_list = list()

    for i, content in enumerate(target):

        meta, video = content
        if commen.KEY_ORIGINAL_VIDEO not in meta:
            meta[commen.KEY_ORIGINAL_VIDEO] = og_video
        kwargs = {commen.KEY_VIDEO_FPS: fps, commen.KEY_VIDEO_ENDING: ext}
        meta_file = commen.save(output_dir, parent_name + "_S" + str(i), meta, video, **kwargs)

        meta_list.append(meta_file)

    return meta_list


def run(config_path, target_file):
    # if len(sys.argv) != 3:
    #     print("need exactly 2 args (config-path) and (taget_file)")
    #     return

    with open(config_path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)


    my_key = "scene_splitter"
    if my_key not in data:
        print("key (" + my_key + " ) is missing in " + config_path)
        return -1

    if not os.path.exists(target_file):
        print("file: " + str(target_file) + " not found")

    data = data[my_key]
    remove_old = data[commen.KEY_REMOVE_OLD] if commen.KEY_REMOVE_OLD in data else False

    parent_name, parent_ext = os.path.splitext(os.path.split(target_file)[-1])
    output_dir = os.path.join(data["output_dir"], parent_name)

    new_og_video = os.path.join(output_dir, parent_name + parent_ext)
    print(output_dir)
    os.makedirs(output_dir, exist_ok=True)

    shutil.copy(target_file, new_og_video)

    cap = cv2.VideoCapture(new_og_video)
    fps = cap.get(cv2.CAP_PROP_FPS)

    cut_scenes(cap, new_og_video, fps=fps, **data)

    if remove_old:
        os.remove(target_file)

    # save_vid(scenes, output_dir, parent_name, fps, new_og_video)


def some_test():
    vid = """/media/ubuntu/data/lip_reading_data/downloads/1LqXIkcqvog.mp4"""
    conf = """./config.yaml"""

    run(conf, vid)

def main():

    if len(sys.argv) != 3:
        print("need exactly 2 args (config-path) and (taget_file)")

    config_path = sys.argv[1]
    target_file = sys.argv[2]

    run(config_path, target_file)

if __name__ == '__main__':
    main()
